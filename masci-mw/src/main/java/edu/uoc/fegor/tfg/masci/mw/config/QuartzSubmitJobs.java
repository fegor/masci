/*
 *****************************************************************************
 * MASCI
 *****************************************************************************
 * Copyright (C) 2019-2020 MASCI authors
 *
 * Authors: Fernando González Ruano [fegor@uoc.edu]
 *
 * This file is part of the Alfresco software. 
 * If the software was purchased under a paid Alfresco license, the terms of 
 * the paid license agreement will prevail.  Otherwise, the software is 
 * provided under the following open source license terms:
 * 
 * Alfresco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Alfresco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Alfresco. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.uoc.fegor.tfg.masci.mw.config;

import org.quartz.JobDetail;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;

import edu.uoc.fegor.tfg.masci.mw.quartz.AlfrescoAuditJob;

/**
 * @author FeGoR [fegor@uoc.edu]
 * @version 01.00.000
 * @created 21 nov. 2019
 * @modified 21 nov. 2019 by FeGoR [fegor@uoc.edu]
 * @file QuartzSubmitJobs.java
 *
 */
@Configuration
public class QuartzSubmitJobs {

	@Value("${alfresco.audit.cron}")
	private String cronAlfrescoAuditJob;
	
    /**
     * @return
     */
    @Bean(name = "jobAlfrescoAudit")
    public JobDetailFactoryBean jobAlfrescoAudit() {

        return QuartzConfig.createJobDetail(AlfrescoAuditJob.class, "Class Alfresco Audit Job");
    }
    
    /**
     * @param jobDetail
     * @return
     */
    @Bean(name = "alfrescoAuditTrigger")
    public CronTriggerFactoryBean alfrescoAuditTrigger(@Qualifier("jobAlfrescoAudit") JobDetail jobDetail) {
    	
        return QuartzConfig.createCronTrigger(jobDetail, this.cronAlfrescoAuditJob, "Class Alfresco Audit Trigger");
    }
}
