/*
 *****************************************************************************
 * MASCI
 *****************************************************************************
 * Copyright (C) 2019-2020 MASCI authors
 *
 * Authors: Fernando González Ruano [fegor@uoc.edu]
 *
 * This file is part of the Alfresco software. 
 * If the software was purchased under a paid Alfresco license, the terms of 
 * the paid license agreement will prevail.  Otherwise, the software is 
 * provided under the following open source license terms:
 * 
 * Alfresco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Alfresco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Alfresco. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.uoc.fegor.tfg.masci.mw.services;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.uoc.fegor.tfg.masci.mw.domain.alfresco.People;
import edu.uoc.fegor.tfg.masci.mw.factory.RestTemplateFactory;

/**
 * @author FeGoR [fegor@uoc.edu]
 * @version 01.00.000
 * @created 21 nov. 2019
 * @modified 21 nov. 2019 by FeGoR [fegor@uoc.edu]
 * @file AlfrescoPeopleService.java
 *
 *       Servicio de usuarios en Alfresco.
 */
@Service
public class AlfrescoPeopleService {
	private static final Logger LOG = LoggerFactory.getLogger(AlfrescoPeopleService.class);

	// Datos de configuración.

	@Value("${alfresco.people.url}")
	private String peopleUrl;

	// Plantilla para RESTful.

	@Autowired
	private RestTemplateFactory restTemplateFactory;

	// Mapeado del objeto People.

	private ObjectMapper mapper;
	private TypeReference<People> typeReference;

	/**
	 * Constructor.
	 */
	public AlfrescoPeopleService() {
		super();
		mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		typeReference = new TypeReference<People>() {
		};
	}

	/**
	 * @param userName
	 * @return
	 * 
	 * 		Recoge un objeto Poeple de un nombre de usuario.
	 */
	public People getPeople(String userName) {
		return this.mapPeople(restTemplateFactory.getString(this.peopleUrl + userName));
	}

	/**
	 * @param s
	 * @return
	 * 
	 * 		Mapeo de los datos de usuario en el objeto People.
	 */
	private People mapPeople(String s) {
		People people = null;

		try {
			people = mapper.readValue(s, typeReference);

		} catch (IOException e) {
			LOG.error("Error in mapping people: {}", e.getMessage());
		}

		return people;
	}
}
