/*
 *****************************************************************************
 * MASCI
 *****************************************************************************
 * Copyright (C) 2019-2020 MASCI authors
 *
 * Authors: Fernando González Ruano [fegor@uoc.edu]
 *
 * This file is part of the Alfresco software. 
 * If the software was purchased under a paid Alfresco license, the terms of 
 * the paid license agreement will prevail.  Otherwise, the software is 
 * provided under the following open source license terms:
 * 
 * Alfresco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Alfresco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Alfresco. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.uoc.fegor.tfg.masci.mw.domain.neo4j;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

/**
 * @author FeGoR [fegor@uoc.edu]
 * @version 01.00.000
 * @created 1 dic. 2019
 * @modified 1 dic. 2019 by FeGoR [fegor@uoc.edu]
 * @file UpdatedContainer.java
 *
 */
@RelationshipEntity(type = "UPDATED")
public class UpdatedContainer {

	@Id
	@GeneratedValue
	private Long id;
	private List<Date> updatedContainer = new ArrayList<>();

	@StartNode
	private User user;
	
	@EndNode
	private Container container;

	public UpdatedContainer() {
		super();
	}

	public UpdatedContainer(User user, Container container) {
		super();
		this.user = user;
		this.container = container;
	}
	
	/**
	 * @param name
	 */
	public void addUpdatedContainer(Date date) {
		if (this.updatedContainer == null) {
			this.updatedContainer = new ArrayList<>();
		}

		this.updatedContainer.add(date);
	}

	public Long getId() {
		return id;
	}

	public List<Date> getUpdatedContainer() {
		return updatedContainer;
	}

	public User getUser() {
		return user;
	}

	public Container getContainer() {
		return container;
	}
}
