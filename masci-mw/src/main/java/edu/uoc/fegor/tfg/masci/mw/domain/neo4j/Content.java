/*
 *****************************************************************************
 * MASCI
 *****************************************************************************
 * Copyright (C) 2019-2020 MASCI authors
 *
 * Authors: Fernando González Ruano [fegor@uoc.edu]
 *
 * This file is part of the Alfresco software. 
 * If the software was purchased under a paid Alfresco license, the terms of 
 * the paid license agreement will prevail.  Otherwise, the software is 
 * provided under the following open source license terms:
 * 
 * Alfresco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Alfresco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Alfresco. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.uoc.fegor.tfg.masci.mw.domain.neo4j;

import java.util.ArrayList;
import java.util.List;

import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

/**
 * @author FeGoR [fegor@uoc.edu]
 * @version 01.00.000
 * @created 3 nov. 2019
 * @modified 4 nov. 2019 by FeGoR [fegor@uoc.edu]
 * @filename Content.java
 *
 */
@NodeEntity
public class Content {
	
	@Id
	@GeneratedValue
	private Long id;
	private String uuid;
	private String name;
	private String title;
	private String path;
	
//	@JsonIgnoreProperties("createdContent")
	@Relationship(type = "CREATED", direction = Relationship.INCOMING)
	private List<CreatedContent> createdContent;

//	@JsonIgnoreProperties("readedContent")
	@Relationship(type = "READED", direction = Relationship.INCOMING)
	private List<ReadedContent> readedContent;
	
//	@JsonIgnoreProperties("containedIn")
	@Relationship(type = "CONTAINED_IN", direction = Relationship.OUTGOING)
	private List<ContainedIn> containedIn;
	
//	@JsonIgnoreProperties("updatedContent")
	@Relationship(type = "UPDATED", direction = Relationship.INCOMING)
	private List<UpdatedContent> updatedContent;
	
	/**
	 * 
	 */
	public Content() {
		super();
	}
	
	/**
	 * @param uuid
	 * @param name
	 * @param title
	 * @param path
	 */
	public Content(String uuid, String name, String title, String path) {
		super();
		this.uuid = uuid;
		this.name = name;
		this.title = title;
		this.path = path;
	}
	
	/**
	 * @param createdContent
	 */
	public void addCreatedContent(CreatedContent createdContent) {
		if (this.createdContent == null) {
			this.createdContent = new ArrayList<>();
		}

		this.createdContent.add(createdContent);
	}

	/**
	 * @param readedContent
	 */
	public void addReadedContent(ReadedContent readedContent) {
		if (this.readedContent == null) {
			this.readedContent = new ArrayList<>();
		}

		this.readedContent.add(readedContent);
	}

	/**
	 * @param containedIn
	 */
	public void addContainedIn(ContainedIn containedIn) {
		if (this.containedIn == null) {
			this.containedIn = new ArrayList<>();
		}

		this.containedIn.add(containedIn);
	}

	public void addUpdatedContent(UpdatedContent updatedContent) {
		if (this.updatedContent == null) {
			this.updatedContent = new ArrayList<>();
		}

		this.updatedContent.add(updatedContent);
	}
	
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Long getId() {
		return id;
	}

	public String getUuid() {
		return uuid;
	}

	public String getName() {
		return name;
	}

	public String getTitle() {
		return title;
	}

	public String getPath() {
		return path;
	}
	
	public List<CreatedContent> getCreatedContent() {
		return createdContent;
	}
	
	public List<ReadedContent> getReadedContent() {
		return readedContent;
	}
	
	public List<ContainedIn> getContainedIn() {
		return containedIn;
	}
	
	public List<UpdatedContent> getUpdatedContent() {
		return updatedContent;
	}
}
