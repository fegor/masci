/*
 *****************************************************************************
 * MASCI
 *****************************************************************************
 * Copyright (C) 2019-2020 MASCI authors
 *
 * Authors: Fernando González Ruano [fegor@uoc.edu]
 *
 * This file is part of the Alfresco software. 
 * If the software was purchased under a paid Alfresco license, the terms of 
 * the paid license agreement will prevail.  Otherwise, the software is 
 * provided under the following open source license terms:
 * 
 * Alfresco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Alfresco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Alfresco. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.uoc.fegor.tfg.masci.mw.services;

import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.uoc.fegor.tfg.masci.mw.domain.alfresco.audit.Audit;
import edu.uoc.fegor.tfg.masci.mw.factory.RestTemplateFactory;

/**
 * @author FeGoR [fegor@uoc.edu]
 * @version 01.00.000
 * @created 21 nov. 2019
 * @modified 21 nov. 2019 by FeGoR [fegor@uoc.edu]
 * @file AlfrescoAuditService.java
 *
 *       Gestiona los servicios de auditoría con Alfresco.
 */
@Service
public class AlfrescoAuditService {
	private static final Logger LOG = LoggerFactory.getLogger(AlfrescoAuditService.class);
	private static final String PARAM_LIMIT = "&limit=";
	private static final String PARAM_FROM_ID = "&fromId=";
	private static final String PARAM_FROM_TIME = "&fromTime=";

	// Variables de configuración.

	@Value("${alfresco.audit.url}")
	private String auditUrl;

	@Value("${alfresco.audit.limit}")
	private int limit;

	// Plantilla para llamada vía RESTful.

	@Autowired
	private RestTemplateFactory restTemplateFactory;

	private ObjectMapper mapper;
	private TypeReference<Audit> typeReference;

	/**
	 * Constructor
	 */
	public AlfrescoAuditService() {
		super();

		// mapeo para el objeto Audit.

		mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		typeReference = new TypeReference<Audit>() {
		};
	}

	/**
	 * @return
	 * 
	 * 		Llamada a la auditoría para recoger todos los items.
	 */
	public Audit getAllAuditAlfresco() {
		return this.mapAudit(restTemplateFactory.getString(this.auditUrl + PARAM_LIMIT + this.limit));
	}

	/**
	 * @param id
	 * @return
	 * 
	 * 		Llamada a la auditoría para recoger items desde un identificador
	 *         determinado.
	 */
	public Audit getAuditAlfrescoFromId(String id) {
		String url = this.auditUrl + PARAM_FROM_ID + id + PARAM_LIMIT + this.limit;
		return this.mapAudit(restTemplateFactory.getString(url));
	}

	/**
	 * @param time
	 * @return
	 * 
	 * 		Llamada a la auditoría desde una fecha dada.
	 */
	public Audit getAuditAlfrescoFromTime(String time) {
		String url = this.auditUrl + PARAM_FROM_TIME + time + "&limit=" + this.limit;
		return this.mapAudit(restTemplateFactory.getString(url));
	}

	/**
	 * @param s
	 * @return
	 * 
	 * 		Mapea el resultado devuelto por Alfresco.
	 */
	private Audit mapAudit(String s) {
		Audit audit = null;

		try {
			audit = mapper.readValue(s, typeReference);

		} catch (IOException e) {
			LOG.error("Error in mapping audit: {}", e.getMessage());
		}

		return audit;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

}
