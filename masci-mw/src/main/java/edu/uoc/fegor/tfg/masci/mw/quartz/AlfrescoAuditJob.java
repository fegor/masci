/*
 *****************************************************************************
 * MASCI
 *****************************************************************************
 * Copyright (C) 2019-2020 MASCI authors
 *
 * Authors: Fernando González Ruano [fegor@uoc.edu]
 *
 * This file is part of the Alfresco software. 
 * If the software was purchased under a paid Alfresco license, the terms of 
 * the paid license agreement will prevail.  Otherwise, the software is 
 * provided under the following open source license terms:
 * 
 * Alfresco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Alfresco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Alfresco. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.uoc.fegor.tfg.masci.mw.quartz;

import java.text.ParseException;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import edu.uoc.fegor.tfg.masci.mw.domain.alfresco.audit.Access;
import edu.uoc.fegor.tfg.masci.mw.domain.alfresco.audit.Audit;
import edu.uoc.fegor.tfg.masci.mw.domain.neo4j.LastAudit;
import edu.uoc.fegor.tfg.masci.mw.model.AlfrescoModel;
import edu.uoc.fegor.tfg.masci.mw.neo4j.Neo4jManager;
import edu.uoc.fegor.tfg.masci.mw.services.AlfrescoAuditService;
import edu.uoc.fegor.tfg.masci.mw.services.LastAuditService;

/**
 * @author FeGoR [fegor@uoc.edu]
 * @version 01.00.000
 * @created 11 nov. 2019
 * @modified 11 nov. 2019 by FeGoR [fegor@uoc.edu]
 * @filename AlfrescoAuditJob.java
 *
 *           Componente de ejecución de trabajo para recoger los datos de la
 *           auditoría.
 */
@Component
@DisallowConcurrentExecution
public class AlfrescoAuditJob implements Job {
	private static final Logger LOG = LoggerFactory.getLogger(AlfrescoAuditJob.class);
	private static final String TYPE_NOT_FOUND = "A type could not be found to execute in the graph database.";
	private static final String COMMAND_NOT_FOUND = "A command could not be found to execute in the graph database.";

	@Autowired
	private Neo4jManager neo4jManager;

	@Autowired
	private AlfrescoAuditService alfrescoAuditService;

	@Autowired
	private LastAuditService lastAuditService;

	public AlfrescoAuditJob() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {

		Audit audit = null;
		String id = null;
		String fromDate = null;

		LastAudit lastAudit = this.readLastAudit();

		// Si no existe un registro de último identificador en la auditoría, se leen
		// todos los registros hasta el límite "limit". En caso contrario, se lee desde
		// el último identificador guardado.

		if (lastAudit.getAccessId().equals("-1")) {
			id = "-1";
			audit = alfrescoAuditService.getAllAuditAlfresco();

		} else {
			id = lastAudit.getAccessId();
			audit = alfrescoAuditService.getAuditAlfrescoFromId(id);
		}

		if (!audit.getEntries().isEmpty()) {
			Access access = audit.getEntries().get(audit.getEntries().size() - 1);

			// Si el identificador guardado es igual o mayor que el recogido se sale del
			// método para esperar a que se produzcan más entradas en la auditoría.

			if (Long.parseLong(id) >= access.getId()) {
				if (LOG.isDebugEnabled()) {
					LOG.debug("No data available");
				}

				return;
			}

			try {
				executeActions(audit);

			} catch (ParseException e) {
				LOG.error("Parse error!");
			}

			id = String.valueOf(access.getId() + 1);
			fromDate = access.getTime();

			lastAudit.setAccessId(id);
			lastAudit.setFromDate(fromDate);

			this.saveLastAudit(lastAudit);
		}
	}

	/**
	 * @param access
	 * @throws ParseException
	 * 
	 *                        Ejecución de las acciones que se han recuperado de la
	 *                        auditoría.
	 */
	private void executeActions(Audit audit) throws ParseException {
		for (Access a : audit.getEntries()) {
			String user = a.getUser();

			if (!user.equals("null")) {
				if (LOG.isDebugEnabled() && a.getValues().getAction() != null) {
					LOG.debug("Access: [Id = {}, User = {}, Action = {}, Type = {}, Path = {}]", a.getId(), a.getUser(),
							a.getValues().getAction(), a.getValues().getType(), a.getValues().getPath());
				}

				this.caseActions(a);
			}
		}
	}

	/**
	 * @param a
	 * @throws ParseException
	 * 
	 *                        Selecciona según las acciones.
	 */
	private void caseActions(Access a) throws ParseException {
		String action = a.getValues().getAction();

		// Solo si existe una acción.

		if (action != null) {
			if (action.equals(AlfrescoModel.ALF_AUDIT_ACTION_CREATE)) {
				this.createActions(a);

			} else if (action.equals(AlfrescoModel.ALF_AUDIT_ACTION_READ)) {
				this.readActions(a);

			} else if (action.equals(AlfrescoModel.ALF_AUDIT_ACTION_UPDATE_CONTENT)
					|| action.equals(AlfrescoModel.ALF_AUDIT_ACTION_UPDATE_NODE_PROPERTIES)
					|| action.equals(AlfrescoModel.ALF_AUDIT_ACTION_MOVE)) {
				this.updateActions(a);

			} else if (action.equals(AlfrescoModel.ALF_AUDIT_ACTION_DELETE)) {
				this.deleteActions(a);

			} else {
				LOG.error(COMMAND_NOT_FOUND);
			}
		}
	}

	/**
	 * @param a
	 * @throws ParseException
	 * 
	 *                        Acciones de creación.
	 */
	private void createActions(Access a) throws ParseException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Create action executed!");
		}

		String type = a.getValues().getType();

		if (type.equals(AlfrescoModel.ALF_AUDIT_PROP_PERSON_USERNAME)) {
			this.neo4jManager.createUser(a);

		} else if (type.equals(AlfrescoModel.ALF_AUDIT_TYPE_CONTENT)) {
			this.neo4jManager.createContent(a);

		} else if (type.equals(AlfrescoModel.ALF_AUDIT_TYPE_FOLDER)) {
			this.neo4jManager.createContainer(a);

		} else {
			LOG.warn(TYPE_NOT_FOUND);
		}
	}

	/**
	 * @param a
	 * @throws ParseException
	 * 
	 *                        Acciones de lectura.
	 */
	private void readActions(Access a) throws ParseException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Read action executed!");
		}

		String type = a.getValues().getType();

		if (type.equals(AlfrescoModel.ALF_AUDIT_TYPE_CONTENT)) {
			this.neo4jManager.readContent(a);

		} else if (type.equals(AlfrescoModel.ALF_AUDIT_TYPE_FOLDER)) {
			this.neo4jManager.readContainer(a);

		} else {
			LOG.warn(TYPE_NOT_FOUND);
		}
	}

	/**
	 * @param a
	 * @throws ParseException
	 * 
	 *                        Acciones de actualización.
	 */
	private void updateActions(Access a) throws ParseException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Update action executed!");
		}

		String type = a.getValues().getType();

		if (type.equals(AlfrescoModel.ALF_AUDIT_TYPE_CONTENT)) {
			this.neo4jManager.updateContent(a);

		} else if (type.equals(AlfrescoModel.ALF_AUDIT_TYPE_FOLDER)) {
			this.neo4jManager.updateContainer(a);

		} else {
			LOG.warn(TYPE_NOT_FOUND);
		}
	}

	/**
	 * @param a
	 * 
	 *          Acciones de borrado.
	 */
	private void deleteActions(Access a) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Delete action executed!");
		}

		String type = a.getValues().getType();

		if (type.equals(AlfrescoModel.ALF_AUDIT_TYPE_CONTENT)) {
			this.neo4jManager.deleteContent(a);

		} else if (type.equals(AlfrescoModel.ALF_AUDIT_TYPE_FOLDER)) {
			this.neo4jManager.deleteContainer(a);

		} else {
			LOG.warn(TYPE_NOT_FOUND);
		}
	}

	/**
	 * @return
	 * 
	 * 		Datos de la última lectura de la auditoría.
	 */
	private LastAudit readLastAudit() {

		LastAudit lastAudit = lastAuditService.getLastAudit();

		if (lastAudit == null) {
			lastAudit = new LastAudit("-1", "1/1/1970");
			lastAuditService.saveLastAudit(lastAudit);
		}

		return lastAudit;
	}

	/**
	 * @param lastAudit
	 */
	private void saveLastAudit(LastAudit lastAudit) {
		lastAuditService.saveLastAudit(lastAudit);
	}
}
