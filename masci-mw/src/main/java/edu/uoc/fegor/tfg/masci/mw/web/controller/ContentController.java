/*
 *****************************************************************************
 * MASCI
 *****************************************************************************
 * Copyright (C) 2019-2020 MASCI authors
 *
 * Authors: Fernando González Ruano [fegor@uoc.edu]
 *
 * This file is part of the Alfresco software. 
 * If the software was purchased under a paid Alfresco license, the terms of 
 * the paid license agreement will prevail.  Otherwise, the software is 
 * provided under the following open source license terms:
 * 
 * Alfresco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Alfresco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Alfresco. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.uoc.fegor.tfg.masci.mw.web.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.uoc.fegor.tfg.masci.mw.services.Neo4jService;
import io.swagger.annotations.Api;

/**
 * @author FeGoR [fegor@uoc.edu]
 * @version 01.00.000
 * @created 3 nov. 2019
 * @modified 3 nov. 2019 by FeGoR [fegor@uoc.edu]
 * @filename ContentController.java
 *
 *           Controlador de servicios de contenido.
 */
@RestController
@RequestMapping("/content")
@Api(value = "Content")
public class ContentController {

	@Autowired
	private final Neo4jService neo4jService;

	/**
	 * @param neo4jService
	 * 
	 *                     Constructor.
	 */
	public ContentController(Neo4jService neo4jService) {
		this.neo4jService = neo4jService;
	}

	/**
	 * @param limit
	 * @return
	 * 
	 * 		Devuelve un map para generar un grafo.
	 */
	@GetMapping("/graph")
	public Map<String, Object> graph(@RequestParam(value = "limit", required = false) Integer limit) {
		return neo4jService.contentGraph(limit == null ? 100 : limit);
	}

	/**
	 * @param limit
	 * @return
	 * 
	 * 		Devuelve el contenido que está contenido en los contenedores.
	 */
	@GetMapping("/contentContainedIn")
	public Map<String, Object> contentContainedIn(@RequestParam(value = "limit", required = false) Integer limit) {
		return neo4jService.contentContainerIn(limit == null ? 100 : limit);
	}
}
