/*
 *****************************************************************************
 * MASCI
 *****************************************************************************
 * Copyright (C) 2019-2020 MASCI authors
 *
 * Authors: Fernando González Ruano [fegor@uoc.edu]
 *
 * This file is part of the Alfresco software. 
 * If the software was purchased under a paid Alfresco license, the terms of 
 * the paid license agreement will prevail.  Otherwise, the software is 
 * provided under the following open source license terms:
 * 
 * Alfresco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Alfresco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Alfresco. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.uoc.fegor.tfg.masci.mw.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.uoc.fegor.tfg.masci.mw.domain.neo4j.ContainedIn;
import edu.uoc.fegor.tfg.masci.mw.domain.neo4j.Container;
import edu.uoc.fegor.tfg.masci.mw.domain.neo4j.Content;
import edu.uoc.fegor.tfg.masci.mw.domain.neo4j.CreatedContainer;
import edu.uoc.fegor.tfg.masci.mw.domain.neo4j.CreatedContent;
import edu.uoc.fegor.tfg.masci.mw.domain.neo4j.ReadedContainer;
import edu.uoc.fegor.tfg.masci.mw.domain.neo4j.ReadedContent;
import edu.uoc.fegor.tfg.masci.mw.domain.neo4j.UpdatedContainer;
import edu.uoc.fegor.tfg.masci.mw.domain.neo4j.UpdatedContent;
import edu.uoc.fegor.tfg.masci.mw.domain.neo4j.User;
import edu.uoc.fegor.tfg.masci.mw.repository.ContainerRepository;
import edu.uoc.fegor.tfg.masci.mw.repository.ContentRepository;
import edu.uoc.fegor.tfg.masci.mw.repository.UserRepository;

/**
 * @author FeGoR [fegor@uoc.edu]
 * @version 01.00.000
 * @created 3 nov. 2019
 * @modified 4 nov. 2019 by FeGoR [fegor@uoc.edu]
 * @filename Neo4jService.java
 *
 *           Servicios proporcionados por Neo4j al middleware.
 */
@Service("Neo4jService")
public class Neo4jService {
	private static final Logger LOG = LoggerFactory.getLogger(Neo4jService.class);
	private static final String MESSAGE_DOCUMENTS = "documents";
	
	// Repositorios de almacenamiento en Neo4j.

	@Autowired
	private ContentRepository contentRepository;

	@Autowired
	private ContainerRepository containerRepository;

	@Autowired
	private UserRepository userRepository;

	/**
	 * @param contents
	 * @return
	 * 
	 * 		Genera un formato de relaciones con nodos.
	 */
	private Map<String, Object> toD3Format(Collection<Content> contents) {
		List<Map<String, Object>> nodes = new ArrayList<>();
		List<Map<String, Object>> rels = new ArrayList<>();

		int i = 0;
		Iterator<Content> result = contents.iterator();

		while (result.hasNext()) {
			Content content = result.next();
			nodes.add(map("name", content.getName(), "label", "content"));

			int target = i;
			i++;

			if (content.getReadedContent() != null) {
				for (ReadedContent readedContent : content.getReadedContent()) {
					Map<String, Object> user = map("name", readedContent.getUser().getName(), "label", "user");
					int source = nodes.indexOf(user);
					if (source == -1) {
						nodes.add(user);
						source = i++;
					}
					rels.add(map("source", source, "target", target));
				}
			}
		}
		return map("nodes", nodes, "links", rels);
	}

	/**
	 * @param key1
	 * @param value1
	 * @param key2
	 * @param value2
	 * @return
	 * 
	 * 		Mapea claves - valor para realizar
	 */
	private Map<String, Object> map(String key1, Object value1, String key2, Object value2) {
		Map<String, Object> result = new HashMap<>(2);
		result.put(key1, value1);
		result.put(key2, value2);
		return result;
	}

	/**
	 * @param uuid
	 * @return
	 * 
	 * 		Devuelve un contenido de un UUID.
	 */
	@Transactional(readOnly = true)
	public Content contentFindByUUID(String uuid) {
		return contentRepository.findByUuid(uuid);
	}

	/**
	 * @param name
	 * @return
	 * 
	 * 		Devuelve un contenido desde un nombre.
	 */
	@Transactional(readOnly = true)
	public Content contentFindByName(String name) {
		return contentRepository.findByName(name);
	}

	/**
	 * @param title
	 * @return
	 * 
	 * 		Devuelve un contenido buscado según el título.
	 */
	@Transactional(readOnly = true)
	public Content contentFindByTitle(String title) {
		return contentRepository.findByTitle(title);
	}

	/**
	 * @param path
	 * @return
	 * 
	 * 		Devuelve un contenido buscado en una cadena PATH.
	 */
	@Transactional(readOnly = true)
	public Content contentFindByPath(String path) {
		Content content = null;

		try {
			content = contentRepository.findByPath(path);

		} catch (IncorrectResultSizeDataAccessException e) {
			LOG.error("Error finding by path: {}", e.getMessage());
		}

		return content;
	}

	/**
	 * @param uuid
	 * @return
	 * 
	 * 		Devuelve contenidos que contenga un UUID.
	 */
	@Transactional(readOnly = true)
	public Collection<Content> contentFindByUUIDLike(String uuid) {
		return contentRepository.findByUuidLike(uuid);
	}

	/**
	 * @param name
	 * @return
	 * 
	 * 		Devuelve contenidos que contenga un nombre.
	 */
	@Transactional(readOnly = true)
	public Collection<Content> contentfindByNameLike(String name) {
		return contentRepository.findByNameLike(name);
	}

	/**
	 * @param title
	 * @return
	 * 
	 * 		Devuelve contenidos que contenga un título.
	 */
	@Transactional(readOnly = true)
	public Collection<Content> contentFindByTitleLike(String title) {
		return contentRepository.findByTitleLike(title);
	}

	/**
	 * @param title
	 * @return
	 * 
	 * 		Devuelve contenidos que contenga un PATH.
	 */
	@Transactional(readOnly = true)
	public Collection<Content> contentFindByPathLike(String path) {
		return contentRepository.findByPathLike(path);
	}

	/**
	 * @param uuid
	 * @param name
	 * @param title
	 * 
	 *              Crea un contenido a partir de los metadatos proporcionados.
	 */
	@Transactional(readOnly = false)
	public Content contentCreate(String uuid, String name, String title, String path) {
		Content content = new Content(uuid, name, title, path);
		contentRepository.save(content);

		if (LOG.isDebugEnabled()) {
			LOG.debug("Content created: uuid = {}, name = {}, title = {}, path = {}", content.getUuid(),
					content.getName(), content.getTitle(), content.getPath());
		}

		return content;
	}

	/**
	 * @param content
	 * @return
	 * 
	 * 		Actualiza un contenido.
	 */
	@Transactional(readOnly = false)
	public Content contentUpdate(Content content) {
		contentRepository.save(content);
		return content;
	}

	/**
	 * @param content
	 * 
	 *                Borra un contenido.
	 */
	@Transactional(readOnly = false)
	public void contentDelete(Content content) {
		contentRepository.delete(content);
	}

	/**
	 * @param uuid
	 * @return
	 * 
	 * 		Devuelve un contenedor con un UUID.
	 */
	@Transactional(readOnly = true)
	public Container containerFindByUUID(String uuid) {
		return containerRepository.findByUuid(uuid);
	}

	/**
	 * @param name
	 * @return
	 * 
	 * 		Devuelve un contenedor con un nombre determinado.
	 */
	@Transactional(readOnly = true)
	public Container containerFindByName(String name) {
		return containerRepository.findByName(name);
	}

	/**
	 * @param title
	 * @return
	 * 
	 * 		Devuelve un contenedor que tenga un título proporcionado.
	 */
	@Transactional(readOnly = true)
	public Container containerFindByTitle(String title) {
		return containerRepository.findByTitle(title);
	}

	/**
	 * @param title
	 * @return
	 * 
	 * 		Devuelve un contenedor que coincida con un PATH.
	 */
	@Transactional(readOnly = true)
	public Container containerFindByPath(String path) {
		Container container = null;

		try {
			container = containerRepository.findByPath(path);

		} catch (IncorrectResultSizeDataAccessException e) {
			LOG.warn("Expected at most once element.");
		}

		return container;
	}

	/**
	 * @param uuid
	 * @return
	 * 
	 * 		Devuelve contenedores que contengan un UUID.
	 */
	@Transactional(readOnly = true)
	public Collection<Container> containerFindByUUIDLike(String uuid) {
		return containerRepository.findByUuidLike(uuid);
	}

	/**
	 * @param name
	 * @return
	 * 
	 * 		Devuelve contenedores que contengan una parte del nombre.
	 */
	@Transactional(readOnly = true)
	public Collection<Container> containerfindByNameLike(String name) {
		return containerRepository.findByNameLike(name);
	}

	/**
	 * @param title
	 * @return
	 * 
	 * 		Devuelve contenedores que contengan una parte del título.
	 */
	@Transactional(readOnly = true)
	public Collection<Container> containerFindByTitleLike(String title) {
		return containerRepository.findByTitleLike(title);
	}

	/**
	 * @param title
	 * @return
	 * 
	 * 		Devuelve contenedores que contengan una cadena PATH.
	 */
	@Transactional(readOnly = true)
	public Collection<Container> containerFindByPathLike(String path) {
		return containerRepository.findByPathLike(path);
	}

	/**
	 * @param uuid
	 * @param name
	 * @param title
	 * 
	 *              Crea un contenedor con los valores proporcionados.
	 */
	@Transactional(readOnly = false)
	public Container containerCreate(String uuid, String name, String title, String path) {
		Container container = new Container(uuid, name, title, path);
		containerRepository.save(container);

		if (LOG.isDebugEnabled()) {
			LOG.debug("Container created: uuid = {}, name = {}, title = {}", container.getUuid(), container.getName(),
					container.getTitle());
		}

		return container;
	}

	/**
	 * @param container
	 * @return
	 * 
	 * 		Actualiza un contenedor.
	 */
	@Transactional(readOnly = false)
	public Container containerUpdate(Container container) {
		containerRepository.save(container);
		return container;
	}

	/**
	 * @param container
	 * 
	 *                  Borra un contenedor.
	 */
	@Transactional(readOnly = false)
	public void containerDelete(Container container) {
		containerRepository.delete(container);
	}

	/**
	 * @param username
	 * @return
	 * 
	 * 		Devuelve un usuario por el nombre de usuario.
	 */
	@Transactional(readOnly = true)
	public User userFindByUsername(String username) {
		return userRepository.findByUsername(username);
	}

	/**
	 * @param title
	 * @return
	 * 
	 * 		Crea un nuevo usuario ocn los datos proporcionados.
	 */
	@Transactional(readOnly = false)
	public User userCreate(String uuid, String username, String name, String surname, String email) {
		User user = new User(uuid, username, name, surname, email);
		userRepository.save(user);

		return user;
	}

	/**
	 * @param user
	 * @param content
	 * 
	 *                Crea una relación entre usuario y contenido creado.
	 */
	@Transactional(readOnly = false)
	public void userCreatedContent(User user, Content content, Date date) {
		CreatedContent createdContent = new CreatedContent(user, content);
		createdContent.addCreatedContent(date);
		content.addCreatedContent(createdContent);
		contentRepository.save(content);
	}

	/**
	 * @param user
	 * @param content
	 * 
	 *                Crea una relación entre un usuario y un contenido leído.
	 */
	@Transactional(readOnly = false)
	public void userReadedContent(User user, Content content, Date date) {
		ReadedContent readedContent = new ReadedContent(user, content);
		readedContent.addReadedContent(date);
		content.addReadedContent(readedContent);
		contentRepository.save(content);
	}

	/**
	 * @param user
	 * @param content
	 * 
	 *                Crea una relación entre un usuario y un contenido actualizado.
	 */
	@Transactional(readOnly = false)
	public void userUpdatedContent(User user, Content content, Date date) {
		UpdatedContent updatedContent = new UpdatedContent(user, content);
		updatedContent.addUpdatedContent(date);
		content.addUpdatedContent(updatedContent);
		contentRepository.save(content);
	}

	/**
	 * @param user
	 * @param container
	 * 
	 *                  Crea una relación entre un usuario y un contenedor creado.
	 */
	@Transactional(readOnly = false)
	public void userCreatedContainer(User user, Container container, Date date) {
		CreatedContainer createdContainer = new CreatedContainer(user, container);
		createdContainer.addCreatedContainer(date);
		container.addCreatedContainer(createdContainer);
		containerRepository.save(container);
	}

	/**
	 * @param user
	 * @param container
	 * 
	 *                  Crea una relación entre un usuario y un contenedor
	 *                  actualizado.
	 */
	@Transactional(readOnly = false)
	public void userUpdatedContainer(User user, Container container, Date date) {
		UpdatedContainer updatedContainer = new UpdatedContainer(user, container);
		updatedContainer.addUpdatedContainer(date);
		container.addUpdatedContainer(updatedContainer);
		containerRepository.save(container);
	}

	/**
	 * @param user
	 * @param container
	 * 
	 *                  Crea una relación entre un usuario y un contenedor leído.
	 */
	@Transactional(readOnly = false)
	public void userReadedContainer(User user, Container container, Date date) {
		ReadedContainer readedContainer = new ReadedContainer(user, container);
		readedContainer.addReadedContainer(date);
		container.addReadedContainer(readedContainer);
		containerRepository.save(container);
	}

	/**
	 * @param container
	 * @param content
	 * 
	 *                  Crea la relación entre un contenedor y un contenido.
	 */
	@Transactional(readOnly = false)
	public void contentContainedInContainer(Container container, Content content) {
		ContainedIn containedIn = new ContainedIn(content, container);
		content.addContainedIn(containedIn);
		contentRepository.save(content);
	}

	/**
	 * @param limit
	 * @return
	 * 
	 * 		Crea un gráfico.
	 */
	@Transactional(readOnly = true)
	public Map<String, Object> contentGraph(int limit) {
		Collection<Content> result = contentRepository.graph(limit);
		return toD3Format(result);
	}

	/**
	 * @param user
	 * @param limit
	 * @return
	 * 
	 * 		Crea una recomendación de contenidos según el resto de usuarios los
	 *         haya creado, leído y/o actualizado.
	 */
	@Transactional(readOnly = true)
	public Map<String, Object> recomendationContentCRUDForOtherUsers(String user, int limit) {
		List<Content> items = new ArrayList<>();
		Collection<Content> contents = contentRepository.recomendationContentCRUDForOtherUsers(user, limit);

		Iterator<Content> result = contents.iterator();

		while (result.hasNext()) {
			Content content = result.next();
			items.add(content);
		}

		Map<String, Object> map = new HashMap<>(1);
		map.put(MESSAGE_DOCUMENTS, items);

		return map;
	}

	/**
	 * @param user
	 * @param limit
	 * @return
	 * 
	 * 		Devuelve un listado de contenidos recomendados que han leído otros
	 *         usuarios.
	 */
	@Transactional(readOnly = true)
	public Map<String, Object> recomendationContentReadedForOtherUsers(String user, int limit) {
		List<Content> items = new ArrayList<>();
		Collection<Content> contents = contentRepository.recomendationContentReadedForOtherUsers(user, limit);

		Iterator<Content> result = contents.iterator();

		while (result.hasNext()) {
			Content content = result.next();
			items.add(content);
		}

		Map<String, Object> map = new HashMap<>(1);
		map.put(MESSAGE_DOCUMENTS, items);

		return map;
	}

	/**
	 * @param limit
	 * @return
	 * 
	 * 		Devuelve contenidos que están contenidos en el mismo contenedor.
	 */
	@Transactional(readOnly = true)
	public Map<String, Object> contentContainerIn(int limit) {
		List<Content> items = new ArrayList<>();
		Collection<Content> contents = contentRepository.contentContainedIn(limit);

		Iterator<Content> result = contents.iterator();

		while (result.hasNext()) {
			Content content = result.next();
			items.add(content);
		}

		Map<String, Object> map = new HashMap<>(1);
		map.put(MESSAGE_DOCUMENTS, items);

		return map;
	}

	/**
	 * @param user
	 * @param limit
	 * @return
	 * 
	 * 		Contenido recomendado para devolver al dashlet de Alfresco.
	 */
	@Transactional(readOnly = true)
	public Map<String, Object> contentRecomendated(String user, int limit) {
		return this.recomendationContentCRUDForOtherUsers(user, limit);
	}
}
