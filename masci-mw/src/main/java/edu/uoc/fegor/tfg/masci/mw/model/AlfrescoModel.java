/*
 *****************************************************************************
 * MASCI
 *****************************************************************************
 * Copyright (C) 2019-2020 MASCI authors
 *
 * Authors: Fernando González Ruano [fegor@uoc.edu]
 *
 * This file is part of the Alfresco software. 
 * If the software was purchased under a paid Alfresco license, the terms of 
 * the paid license agreement will prevail.  Otherwise, the software is 
 * provided under the following open source license terms:
 * 
 * Alfresco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Alfresco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Alfresco. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.uoc.fegor.tfg.masci.mw.model;

public interface AlfrescoModel {

	public final static String ALF_WS_PROTOCOL = "workspace://SpacesStore/";

	public final static String ALF_AUDIT_ACTION_CREATE = "CREATE";
	public final static String ALF_AUDIT_ACTION_UPDATE_CONTENT = "UPDATE CONTENT";
	public final static String ALF_AUDIT_ACTION_UPDATE_NODE_PROPERTIES = "updateNodeProperties";
	public final static String ALF_AUDIT_ACTION_ADD_NODE_ASPECT = "addNodeAspect";
	public final static String ALF_AUDIT_ACTION_READ = "READ";
	public final static String ALF_AUDIT_ACTION_DELETE = "DELETE";
	public final static String ALF_AUDIT_ACTION_MOVE = "MOVE";
	
	public final static String ALF_AUDIT_TYPE_CONTENT = "cm:content";
	public final static String ALF_AUDIT_TYPE_FOLDER = "cm:folder";
	public final static String ALF_AUDIT_TYPE_PERSON = "cm:person";	
	public final static String ALF_AUDIT_TYPE_USER = "usr:user";
	
	public final static String ALF_CONTENT_MODEL = "{http://www.alfresco.org/model/content/1.0}";
	public final static String ALF_SYSTEM_MODEL = "{http://www.alfresco.org/model/system/1.0}";
	public final static String ALF_USER_MODEL = "{http://www.alfresco.org/model/user/1.0}";
	
	public final static String ALF_AUDIT_PROP_UUID = ALF_SYSTEM_MODEL + "node-uuid";
	public final static String ALF_AUDIT_PROP_NAME = ALF_CONTENT_MODEL + "name";
	public final static String ALF_AUDIT_PROP_TITLE = ALF_CONTENT_MODEL + "title";
	public final static String ALF_AUDIT_PROP_USER_USERNAME = ALF_USER_MODEL + "username";
	public final static String ALF_AUDIT_PROP_PERSON_USERNAME = ALF_CONTENT_MODEL + "userName";
	public final static String ALF_AUDIT_PROP_EMAIL = ALF_CONTENT_MODEL + "email";
	public final static String ALF_AUDIT_PROP_FIRSTNAME = ALF_CONTENT_MODEL + "firstName";
	public final static String ALF_AUDIT_PROP_LASTNAME = ALF_CONTENT_MODEL + "lastName";
	
	// FORNEXTVER Crear más valores para todas las propiedades posibles.
}
