/*
 *****************************************************************************
 * MASCI
 *****************************************************************************
 * Copyright (C) 2019-2020 MASCI authors
 *
 * Authors: Fernando González Ruano [fegor@uoc.edu]
 *
 * This file is part of the Alfresco software. 
 * If the software was purchased under a paid Alfresco license, the terms of 
 * the paid license agreement will prevail.  Otherwise, the software is 
 * provided under the following open source license terms:
 * 
 * Alfresco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Alfresco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Alfresco. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.uoc.fegor.tfg.masci.mw.repository;

import java.util.Collection;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import edu.uoc.fegor.tfg.masci.mw.domain.neo4j.Content;

/**
 * @author FeGoR [fegor@uoc.edu]
 * @version 01.00.000
 * @created 3 nov. 2019
 * @modified 4 nov. 2019 by FeGoR [fegor@uoc.edu]
 * @filename ContentRepository.java
 * 
 *           Repositorio para almacenar la información de los contenidos como
 *           nodos en Neo4j.
 */
@RepositoryRestResource(collectionResourceRel = "contents", path = "contents")
public interface ContentRepository extends Neo4jRepository<Content, Long> {

	public Content findByUuid(@Param("uuid") String uuid);

	public Content findByName(@Param("name") String name);

	public Content findByTitle(@Param("title") String title);

	public Content findByPath(@Param("path") String path);

	public Collection<Content> findByUuidLike(@Param("uuid") String uuid);

	public Collection<Content> findByNameLike(@Param("name") String name);

	public Collection<Content> findByTitleLike(@Param("title") String title);

	public Collection<Content> findByPathLike(@Param("path") String path);

	@Query("MATCH (u:User)-[r:READED]->(c:Content) RETURN c,r,u LIMIT {limit}")
	public Collection<Content> graph(@Param("limit") int limit);

	@Query("MATCH p=()-[r:CONTAINED_IN]->() RETURN p LIMIT {limit}")
	public Collection<Content> contentContainedIn(@Param("limit") int limit);

	// Recomendaciones sobre documentos leídos por otros usuarios y que están en el
	// mismo contenedor.

//	@Query("MATCH (user:User)-[r1]->(cont:Content)-[r2]->(container:Container)<-[r3]-(other:Content)<-[r4]-(users:User) "
//			+ "WHERE user.username = {user} AND user <> users AND cont <> other AND NOT (user)-[:CREATED]->(other) AND NOT (user)-[:READED]->(other) AND NOT (user)-[:UPDATED]->(other) AND users.username <> 'admin' "
//			+ "RETURN other LIMIT {limit}")
	@Query("MATCH (user:User)-[r1]->(cont:Content)-[r2]->(container:Container)<-[r3]-(other:Content)<-[r4]-(users:User) "
			+ "WHERE user.username = {user} AND user <> users AND cont <> other AND NOT (user)-[:CREATED]->(other) AND NOT (user)-[:READED]->(other) AND NOT (user)-[:UPDATED]->(other) "
			+ "RETURN other LIMIT {limit}")
	public Collection<Content> recomendationContentCRUDForOtherUsers(@Param("user") String user,
			@Param("limit") int limit);

	@Query("MATCH (user:User)-[r1]->(cont:Content)-[r2]->(container:Container)<-[r3]-(other:Content)<-[r4]-(users:User) "
			+ "WHERE user.username = {user} AND user <> users AND cont <> other AND NOT (user)-[:READED]->(other) "
			+ "RETURN other LIMIT {limit}")
	public Collection<Content> recomendationContentReadedForOtherUsers(@Param("user") String user,
			@Param("limit") int limit);

	@Query("MATCH (user:User)-[:CREATED]->(content:Content)<-[:UPDATED]-(users:User) WHERE user.name = {user} RETURN collect(content) LIMIT {limit}")
	public Collection<Content> recomendationContentUpdatedForOtherUsers(@Param("user") String user,
			@Param("limit") int limit);
}
