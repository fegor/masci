/*
 *****************************************************************************
 * MASCI
 *****************************************************************************
 * Copyright (C) 2019-2020 MASCI authors
 *
 * Authors: Fernando González Ruano [fegor@uoc.edu]
 *
 * This file is part of the Alfresco software. 
 * If the software was purchased under a paid Alfresco license, the terms of 
 * the paid license agreement will prevail.  Otherwise, the software is 
 * provided under the following open source license terms:
 * 
 * Alfresco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Alfresco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Alfresco. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.uoc.fegor.tfg.masci.mw.domain.alfresco;

/**
 * @author FeGoR [fegor@uoc.edu]
 * @version 01.00.000
 * @created 16 dic. 2019
 * @modified 16 dic. 2019 by FeGoR [fegor@uoc.edu]
 * @file PeopleItems.java
 *
 *       Clase que contiene los datos de cada unos de los items devueltos por
 *       Alfresco sobre los usuarios.
 */
public class PeopleItems {
	private String url;
	private String userName;
	private boolean enabled;
	private String firstName;
	private String lastName;
	private String jobtitle;
	private String organization;
	private String organizationId;
	private String location;
	private String telephone;
	private String mobile;
	private String email;
	private String companyaddress1;
	private String companyaddress2;
	private String companyaddress3;
	private String companypostcode;
	private String companytelephone;
	private String companyfax;
	private String companyemail;
	private String skype;
	private String instantmsg;
	private String userStatus;
	private String userStatusTime;
	private String googleusername;
	private long quota;
	private long sizeCurrent;
	private boolean emailFeedDisabled;
	private String persondescription;
	private String authorizationStatus;
	private boolean isDeleted;
	private boolean isAdminAuthority;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getJobtitle() {
		return jobtitle;
	}

	public void setJobtitle(String jobtitle) {
		this.jobtitle = jobtitle;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCompanyaddress1() {
		return companyaddress1;
	}

	public void setCompanyaddress1(String companyaddress1) {
		this.companyaddress1 = companyaddress1;
	}

	public String getCompanyaddress2() {
		return companyaddress2;
	}

	public void setCompanyaddress2(String companyaddress2) {
		this.companyaddress2 = companyaddress2;
	}

	public String getCompanyaddress3() {
		return companyaddress3;
	}

	public void setCompanyaddress3(String companyaddress3) {
		this.companyaddress3 = companyaddress3;
	}

	public String getCompanypostcode() {
		return companypostcode;
	}

	public void setCompanypostcode(String companypostcode) {
		this.companypostcode = companypostcode;
	}

	public String getCompanytelephone() {
		return companytelephone;
	}

	public void setCompanytelephone(String companytelephone) {
		this.companytelephone = companytelephone;
	}

	public String getCompanyfax() {
		return companyfax;
	}

	public void setCompanyfax(String companyfax) {
		this.companyfax = companyfax;
	}

	public String getCompanyemail() {
		return companyemail;
	}

	public void setCompanyemail(String companyemail) {
		this.companyemail = companyemail;
	}

	public String getSkype() {
		return skype;
	}

	public void setSkype(String skype) {
		this.skype = skype;
	}

	public String getInstantmsg() {
		return instantmsg;
	}

	public void setInstantmsg(String instantmsg) {
		this.instantmsg = instantmsg;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	public String getUserStatusTime() {
		return userStatusTime;
	}

	public void setUserStatusTime(String userStatusTime) {
		this.userStatusTime = userStatusTime;
	}

	public String getGoogleusername() {
		return googleusername;
	}

	public void setGoogleusername(String googleusername) {
		this.googleusername = googleusername;
	}

	public long getQuota() {
		return quota;
	}

	public void setQuota(long quota) {
		this.quota = quota;
	}

	public long getSizeCurrent() {
		return sizeCurrent;
	}

	public void setSizeCurrent(long sizeCurrent) {
		this.sizeCurrent = sizeCurrent;
	}

	public boolean isEmailFeedDisabled() {
		return emailFeedDisabled;
	}

	public void setEmailFeedDisabled(boolean emailFeedDisabled) {
		this.emailFeedDisabled = emailFeedDisabled;
	}

	public String getPersondescription() {
		return persondescription;
	}

	public void setPersondescription(String persondescription) {
		this.persondescription = persondescription;
	}

	public String getAuthorizationStatus() {
		return authorizationStatus;
	}

	public void setAuthorizationStatus(String authorizationStatus) {
		this.authorizationStatus = authorizationStatus;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public boolean isAdminAuthority() {
		return isAdminAuthority;
	}

	public void setAdminAuthority(boolean isAdminAuthority) {
		this.isAdminAuthority = isAdminAuthority;
	}
}
