/*
 *****************************************************************************
 * MASCI
 *****************************************************************************
 * Copyright (C) 2019-2020 MASCI authors
 *
 * Authors: Fernando González Ruano [fegor@uoc.edu]
 *
 * This file is part of the Alfresco software. 
 * If the software was purchased under a paid Alfresco license, the terms of 
 * the paid license agreement will prevail.  Otherwise, the software is 
 * provided under the following open source license terms:
 * 
 * Alfresco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Alfresco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Alfresco. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.uoc.fegor.tfg.masci.mw.config;

import java.util.Calendar;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.lang3.ArrayUtils;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;
import org.springframework.scheduling.quartz.SpringBeanJobFactory;

import edu.uoc.fegor.tfg.masci.mw.quartz.AutowiringSpringBeanJobFactory;

/**
 * @author FeGoR [fegor@uoc.edu]
 * @version 01.00.000
 * @created 11 nov. 2019
 * @modified 11 nov. 2019 by FeGoR [fegor@uoc.edu]
 * @filename QuartzConfig.java
 *
 */
@Configuration
public class QuartzConfig {
	private static final Logger LOG = LoggerFactory.getLogger(QuartzConfig.class);

	private ApplicationContext applicationContext;
	private DataSource dataSource;

	/**
	 * @param applicationContext
	 * @param dataSource
	 */
	public QuartzConfig(ApplicationContext applicationContext, DataSource dataSource) {

		this.applicationContext = applicationContext;
		this.dataSource = dataSource;
	}

	/**
	 * @return
	 */
	@Bean
	public SpringBeanJobFactory springBeanJobFactory() {

		AutowiringSpringBeanJobFactory jobFactory = new AutowiringSpringBeanJobFactory();
		jobFactory.setApplicationContext(applicationContext);
		return jobFactory;
	}

	/**
	 * @param triggers
	 * @return
	 */
	@Bean
	public SchedulerFactoryBean scheduler(Trigger... triggers) {

		SchedulerFactoryBean schedulerFactory = new SchedulerFactoryBean();

		Properties properties = new Properties();
		properties.setProperty("org.quartz.scheduler.instanceName", "MASCISchedulerInstance");
		properties.setProperty("org.quartz.scheduler.instanceId", "MASCIInstance1");
		schedulerFactory.setOverwriteExistingJobs(true);
		schedulerFactory.setAutoStartup(true);
		schedulerFactory.setQuartzProperties(properties);
		schedulerFactory.setDataSource(dataSource);
		schedulerFactory.setJobFactory(springBeanJobFactory());

		schedulerFactory.setWaitForJobsToCompleteOnShutdown(true);

		if (ArrayUtils.isNotEmpty(triggers)) {
			schedulerFactory.setTriggers(triggers);
		}

		return schedulerFactory;
	}

	/**
	 * @param jobDetail
	 * @param pollFrequencyMs
	 * @param triggerName
	 * @return
	 */
	static SimpleTriggerFactoryBean createTrigger(JobDetail jobDetail, long pollFrequencyMs, String triggerName) {
		
		if (LOG.isDebugEnabled())
			LOG.debug("createTrigger(jobDetail={}, pollFrequencyMs={}, triggerName={})", jobDetail,
					pollFrequencyMs, triggerName);

		SimpleTriggerFactoryBean factoryBean = new SimpleTriggerFactoryBean();
		factoryBean.setJobDetail(jobDetail);
		factoryBean.setStartDelay(0L);
		factoryBean.setRepeatInterval(pollFrequencyMs);
		factoryBean.setName(triggerName);
		factoryBean.setRepeatCount(SimpleTrigger.REPEAT_INDEFINITELY);
		factoryBean.setMisfireInstruction(SimpleTrigger.MISFIRE_INSTRUCTION_RESCHEDULE_NEXT_WITH_REMAINING_COUNT);

		return factoryBean;
	}

	/**
	 * @param jobDetail
	 * @param cronExpression
	 * @param triggerName
	 * @return
	 */
	static CronTriggerFactoryBean createCronTrigger(JobDetail jobDetail, String cronExpression, String triggerName) {

		if (LOG.isDebugEnabled())
			LOG.debug("createCronTrigger(jobDetail={}, cronExpression={}, triggerName={})", jobDetail.toString(),
					cronExpression, triggerName);

		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		CronTriggerFactoryBean factoryBean = new CronTriggerFactoryBean();
		factoryBean.setJobDetail(jobDetail);
		factoryBean.setCronExpression(cronExpression);
		factoryBean.setStartTime(calendar.getTime());
		factoryBean.setStartDelay(0L);
		factoryBean.setName(triggerName);
		factoryBean.setMisfireInstruction(CronTrigger.MISFIRE_INSTRUCTION_DO_NOTHING);

		return factoryBean;
	}

	/**
	 * @param jobClass
	 * @param jobName
	 * @return
	 */
	@SuppressWarnings("unchecked")
	static JobDetailFactoryBean createJobDetail(@SuppressWarnings("rawtypes") Class jobClass, String jobName) {

		LOG.debug("createJobDetail(jobClass={}, jobName={})", jobClass.getName(), jobName);
		JobDetailFactoryBean factoryBean = new JobDetailFactoryBean();
		factoryBean.setName(jobName);
		factoryBean.setJobClass(jobClass);
		factoryBean.setDurability(true);

		return factoryBean;
	}
}
