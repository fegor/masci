package edu.uoc.fegor.tfg.masci.mw.factory;

import org.apache.http.HttpHost;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.support.BasicAuthenticationInterceptor;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class RestTemplateFactory implements FactoryBean<RestTemplate>, InitializingBean {
	private static final Logger LOG = LoggerFactory.getLogger(RestTemplateFactory.class);

	@Value("${alfresco.host}")
	private String host;

	@Value("${alfresco.port}")
	private int port;

	@Value("${alfresco.protocol}")
	private String protocol;

	@Value("${alfresco.auth.user}")
	private String user;

	@Value("${alfresco.auth.password}")
	private String password;

	private RestTemplate restTemplate;

	public String getString(String url) {
		
//		if (LOG.isDebugEnabled()) {
//			LOG.debug(url);
//		}
		
		return restTemplate.getForObject(url, String.class);
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		
		HttpHost httpHost = new HttpHost(this.host, this.port, this.protocol);
		final ClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactoryBasicAuth(httpHost);
		restTemplate = new RestTemplate();
		restTemplate.setRequestFactory(requestFactory);
		restTemplate.getInterceptors().add(new BasicAuthenticationInterceptor(this.user, this.password));
	}

	@Override
	public RestTemplate getObject() throws Exception {
		
		return restTemplate;
	}

	@Override
	public Class<?> getObjectType() {
		
		return RestTemplate.class;
	}

	@Override
	public boolean isSingleton() {
		
		return true;
	}
}
