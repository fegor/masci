/*
 *****************************************************************************
 * MASCI
 *****************************************************************************
 * Copyright (C) 2019-2020 MASCI authors
 *
 * Authors: Fernando González Ruano [fegor@uoc.edu]
 *
 * This file is part of the Alfresco software. 
 * If the software was purchased under a paid Alfresco license, the terms of 
 * the paid license agreement will prevail.  Otherwise, the software is 
 * provided under the following open source license terms:
 * 
 * Alfresco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Alfresco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Alfresco. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.uoc.fegor.tfg.masci.mw.domain.neo4j;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author FeGoR [fegor@uoc.edu]
 * @version 01.00.000
 * @created 1 dic. 2019
 * @modified 1 dic. 2019 by FeGoR [fegor@uoc.edu]
 * @file LastAudit.java
 *
 */
@Entity
public class LastAudit implements Serializable {
	private static final long serialVersionUID = 7907386181175589471L;
	
	@Id
	@GeneratedValue
	private Long id;
	private String accessId;
	private String fromDate;

	public LastAudit() { 
		// Void constructor
	}

	public LastAudit(String accessId, String fromDate) {
		super();
		this.accessId = accessId;
		this.fromDate = fromDate;
	}

	public String getAccessId() {
		return accessId;
	}

	public void setAccessId(String accessId) {
		this.accessId = accessId;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("{id = ");
		sb.append(this.id);
		sb.append(", accessId = ");
		sb.append(this.accessId);
		sb.append(", fromDate = ");
		sb.append(this.fromDate);
		sb.append("}");
		
		return sb.toString();
	}
}
