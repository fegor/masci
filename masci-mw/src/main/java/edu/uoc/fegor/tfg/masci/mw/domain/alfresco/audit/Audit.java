/*
 *****************************************************************************
 * MASCI
 *****************************************************************************
 * Copyright (C) 2019-2020 MASCI authors
 *
 * Authors: Fernando González Ruano [fegor@uoc.edu]
 *
 * This file is part of the Alfresco software. 
 * If the software was purchased under a paid Alfresco license, the terms of 
 * the paid license agreement will prevail.  Otherwise, the software is 
 * provided under the following open source license terms:
 * 
 * Alfresco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Alfresco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Alfresco. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.uoc.fegor.tfg.masci.mw.domain.alfresco.audit;

import java.util.List;

/**
 * @author FeGoR [fegor@uoc.edu]
 * @version 01.00.000
 * @created 13 nov. 2019
 * @modified 13 nov. 2019 by FeGoR [fegor@uoc.edu]
 *
 *           Clase base del fichero JSON de respuesta de la auditoría de
 *           Alfresco.
 */
public class Audit {

	private Long count;
	private List<Access> entries;

	public Audit() {
	}

	public Audit(Long count, List<Access> entries) {
		super();
		this.count = count;
		this.entries = entries;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public List<Access> getEntries() {
		return entries;
	}

	public void setEntries(List<Access> entries) {
		this.entries = entries;
	}

}
