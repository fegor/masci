/*
 *****************************************************************************
 * MASCI
 *****************************************************************************
 * Copyright (C) 2019-2020 MASCI authors
 *
 * Authors: Fernando González Ruano [fegor@uoc.edu]
 *
 * This file is part of the Alfresco software. 
 * If the software was purchased under a paid Alfresco license, the terms of 
 * the paid license agreement will prevail.  Otherwise, the software is 
 * provided under the following open source license terms:
 * 
 * Alfresco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Alfresco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Alfresco. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.uoc.fegor.tfg.masci.mw.services;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.QueryResult;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.data.PropertyData;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import edu.uoc.fegor.tfg.masci.mw.exception.DocumentNotFoundException;
import edu.uoc.fegor.tfg.masci.mw.exception.FolderNotFoundException;

/**
 * @author FeGoR [fegor@uoc.edu]
 * @version 01.00.000
 * @created 30 nov. 2019
 * @modified 30 nov. 2019 by FeGoR [fegor@uoc.edu]
 * @file AlfrescoCmisService.java
 *
 *       Servicio de llamadas vía CMIS a Alfresco.
 */
@Service
public class AlfrescoCmisService {
	private static final Logger LOG = LoggerFactory.getLogger(AlfrescoCmisService.class);
	private static final String MESSAGE_DOC_NOT_REF = " of document does not return a reference";

	@Value("${alfresco.host}")
	private String host;

	@Value("${alfresco.port}")
	private int port;

	@Value("${alfresco.protocol}")
	private String protocol;

	@Value("${alfresco.auth.user}")
	private String user;

	@Value("${alfresco.auth.password}")
	private String pwd;

	@Value("${alfresco.context}")
	private String context;

	@Value("${alfresco.cmis.url}")
	private String cmisUrl;

	private Session session;

	/**
	 * @param host
	 * @param port
	 * @param protocol
	 * @param user
	 * @param pwd
	 * @param context
	 * @param cmisUrl
	 */
	@Autowired
	public AlfrescoCmisService(@Value("${alfresco.host}") final String host, @Value("${alfresco.port}") final int port,
			@Value("${alfresco.protocol}") final String protocol, @Value("${alfresco.auth.user}") final String user,
			@Value("${alfresco.auth.password}") final String pwd, @Value("${alfresco.context}") final String context,
			@Value("${alfresco.cmis.url}") final String cmisUrl) {
		super();
		this.host = host;
		this.port = port;
		this.protocol = protocol;
		this.user = user;
		this.pwd = pwd;
		this.context = context;
		this.cmisUrl = cmisUrl;
		this.createSession();
	}

	/**
	 * @return
	 * 
	 * 		Crea una sesión con los parámetros de usuario, contraseña y URL.
	 */
	public Session createSession() {
		Map<String, String> sessionParameters = new HashMap<>();
		sessionParameters.put(SessionParameter.USER, this.user);
		sessionParameters.put(SessionParameter.PASSWORD, this.pwd);
		sessionParameters.put(SessionParameter.ATOMPUB_URL,
				this.protocol + "://" + this.host + ":" + this.port + this.context + this.cmisUrl);
		sessionParameters.put(SessionParameter.BINDING_TYPE, BindingType.ATOMPUB.value());
		SessionFactory sessionFactory = SessionFactoryImpl.newInstance();
		this.session = sessionFactory.getRepositories(sessionParameters).get(0).createSession();

		return this.session;
	}

	/**
	 * @return
	 */
	public Session getSession() {
		return session;
	}

	/**
	 * @return
	 * 
	 * 		Lista los items de contenido u objetos CmisObject.
	 */
	public List<CmisObject> contentItems() {
		List<CmisObject> objList = new ArrayList<>();
		Folder root = this.session.getRootFolder();

		for (CmisObject obj : root.getChildren()) {
			objList.add(obj);
		}

		return objList;
	}

	/**
	 * @param query
	 * @return
	 * 
	 * 		Devuelve un listado de resultados de una consulta en CMIS.
	 */
	public List<CmisObject> findByCMISQuery(String query) {
		List<CmisObject> objList = new ArrayList<>();
		ItemIterable<QueryResult> results = session.query(query, false);

		for (QueryResult qResult : results) {
			PropertyData<?> propData = qResult.getPropertyById("cmis:objectId");
			String objectId = (String) propData.getFirstValue();
			CmisObject obj = session.getObject(session.createObjectId(objectId));
			objList.add(obj);
		}

		return objList;
	}

	/**
	 * @param id
	 * @return
	 * @throws DocumentNotFoundException
	 * 
	 *                                   Devuelve un documento (content) de un UUID.
	 */
	public Document getDocumentByID(String id) throws DocumentNotFoundException {
		CmisObject obj = session.getObject(id);

		if (obj instanceof Document) {
			return (Document) obj;

		} else {
			throw new DocumentNotFoundException("The id: " + id + MESSAGE_DOC_NOT_REF);
		}
	}

	/**
	 * @param path
	 * @return
	 * @throws DocumentNotFoundException
	 * 
	 *                                   Devuelve el documento (content) de un path.
	 */
	public Document getDocumentByPath(String path) throws DocumentNotFoundException {
		CmisObject obj = session.getObjectByPath(path);

		if (obj instanceof Document) {
			return (Document) obj;

		} else {
			throw new DocumentNotFoundException("The path: " + path + MESSAGE_DOC_NOT_REF);
		}
	}

	/**
	 * @param xPath
	 * @return
	 * @throws DocumentNotFoundException
	 * 
	 *                                   Devuelve un documento (content) de una
	 *                                   cadena XPath.
	 */
	public Document getDocumentByXPath(String xPath) throws DocumentNotFoundException {
		CmisObject obj = this.getCmisObjectByXPath(xPath, "cmis:document");

		if (obj instanceof Document) {
			return (Document) obj;

		} else {
			throw new DocumentNotFoundException("The xpath: " + xPath + MESSAGE_DOC_NOT_REF);
		}
	}

	/**
	 * @param id
	 * @return
	 * 
	 * 		Devuelve una carpeta (folder) de un UUID.
	 */
	public Folder getFolderByID(String id) throws FolderNotFoundException {
		CmisObject obj = session.getObject(id);

		if (obj instanceof Folder) {
			return (Folder) obj;

		} else {
			throw new FolderNotFoundException("The id: " + id + " of folder does not return reference");
		}
	}

	/**
	 * @param path
	 * @return
	 * @throws FolderNotFoundException
	 * 
	 *                                 Devuelve una carpeta (folder) de un path.
	 */
	public Folder getFolderByPath(String path) throws FolderNotFoundException {
		CmisObject obj = session.getObjectByPath(path);

		if (obj instanceof Folder) {
			return (Folder) obj;

		} else {
			throw new FolderNotFoundException("The path " + path + " does not return reference to a folder");
		}
	}

	/**
	 * @param xPath
	 * @return
	 * @throws FolderNotFoundException
	 * 
	 *                                 Devuelve una carpeta (folder) de una cadena
	 *                                 XPath.
	 */
	public Folder getFolderByXPath(String xPath) throws FolderNotFoundException {
		CmisObject obj = this.getCmisObjectByXPath(xPath, "cmis:folder");

		if (obj instanceof Folder) {
			return (Folder) obj;

		} else {
			throw new FolderNotFoundException("The xpath " + xPath + " does not return reference to a folder");
		}
	}

	/**
	 * @param fileName
	 * @param parent
	 * @param content
	 * @param sizeContent
	 * @param mimetype
	 * @param properties
	 * @return
	 * @throws FolderNotFoundException
	 * 
	 *                                 Crea un documento con los parámetros
	 *                                 aportados.
	 */
	public String createDocument(String fileName, Folder parent, InputStream content, long sizeContent, String mimetype,
			Map<String, Object> properties) throws FolderNotFoundException {
		Folder folder = this.getFolderByID(parent.getId());

		// Se crea el stream del contenido.

		ContentStream contentStream = session.getObjectFactory().createContentStream(fileName, sizeContent, mimetype,
				content);

		// Creación del nodo.

		Document document = folder.createDocument(properties, contentStream, VersioningState.MAJOR);

		return document.getId();
	}

	/**
	 * @param xPath
	 * @return
	 * 
	 * 		Devuelve un objeto CMIS según una búsqueda con una cadena XPath.
	 */
	private CmisObject getCmisObjectByXPath(String xPath, String cmisObject) {
		final String query = "SELECT * FROM " + cmisObject + " WHERE CONTAINS('PATH:\\\"" + xPath + "\\\"')";

		if (LOG.isDebugEnabled()) {
			LOG.debug("Query for XPath: {}", query);
		}

		List<CmisObject> results = this.findByCMISQuery(query);

		if (!results.isEmpty()) {
			if (results.get(0) instanceof Document) {
				return (Document) results.get(0);

			} else {
				return (Folder) results.get(0);
			}

		} else {
			return null;
		}
	}
}
