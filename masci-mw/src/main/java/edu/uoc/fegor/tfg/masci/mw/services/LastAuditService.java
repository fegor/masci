/*
 *****************************************************************************
 * MASCI
 *****************************************************************************
 * Copyright (C) 2019-2020 MASCI authors
 *
 * Authors: Fernando González Ruano [fegor@uoc.edu]
 *
 * This file is part of the Alfresco software. 
 * If the software was purchased under a paid Alfresco license, the terms of 
 * the paid license agreement will prevail.  Otherwise, the software is 
 * provided under the following open source license terms:
 * 
 * Alfresco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Alfresco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Alfresco. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.uoc.fegor.tfg.masci.mw.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.uoc.fegor.tfg.masci.mw.domain.neo4j.LastAudit;
import edu.uoc.fegor.tfg.masci.mw.repository.LastAuditRepository;

/**
 * @author FeGoR [fegor@uoc.edu]
 * @version 01.00.000
 * @created 16 dic. 2019
 * @modified 16 dic. 2019 by FeGoR [fegor@uoc.edu]
 * @file LastAuditService.java
 *
 *       Servicio de almacenamiento de datos de la última llamada a la
 *       auditoría.
 */
@Service
public class LastAuditService {

	@Autowired
	private LastAuditRepository lastAuditRepository;

	/**
	 * @return
	 * 
	 * 		Devuelve los datos de la última auditoría.
	 */
	public LastAudit getLastAudit() {
		List<LastAudit> lastAudits = (List<LastAudit>) lastAuditRepository.findAll();

		if (lastAudits.isEmpty()) {
			return null;

		} else {
			return lastAudits.get(0);
		}
	}

	/**
	 * @param lastAudit
	 * 
	 *                  Guarda los datos de la última auditoría.
	 */
	public void saveLastAudit(LastAudit lastAudit) {
		lastAuditRepository.save(lastAudit);
	}

}
