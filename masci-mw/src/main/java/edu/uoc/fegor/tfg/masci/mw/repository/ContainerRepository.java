package edu.uoc.fegor.tfg.masci.mw.repository;

import java.util.Collection;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import edu.uoc.fegor.tfg.masci.mw.domain.neo4j.Container;

/**
 * @author FeGoR [fegor@uoc.edu]
 * @version 01.00.000
 * @created 4 nov. 2019
 * @modified 4 nov. 2019 by FeGoR [fegor@uoc.edu]
 * @filename ContainerRepository.java
 * 
 *           Repositorio para almacenar como nodos en Neo4j de los contenedores.
 */
@RepositoryRestResource(collectionResourceRel = "containers", path = "containers")
public interface ContainerRepository extends Neo4jRepository<Container, Long> {

	public Container findByUuid(@Param("uuid") String uuid);

	public Container findByName(@Param("name") String name);

	public Container findByTitle(@Param("title") String title);

	public Container findByPath(@Param("path") String path);

	public Collection<Container> findByUuidLike(@Param("uuid") String uuid);

	public Collection<Container> findByNameLike(@Param("name") String name);

	public Collection<Container> findByTitleLike(@Param("title") String title);

	public Collection<Container> findByPathLike(@Param("path") String path);

	@Query("MATCH (f:Container)<-[r:CREATED]-(c:Content) RETURN f,r,c LIMIT {limit}")
	public Collection<Container> graph(@Param("limit") int limit);
}
