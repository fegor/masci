/*
 *****************************************************************************
 * MASCI
 *****************************************************************************
 * Copyright (C) 2019-2020 MASCI authors
 *
 * Authors: Fernando González Ruano [fegor@uoc.edu]
 *
 * This file is part of the Alfresco software. 
 * If the software was purchased under a paid Alfresco license, the terms of 
 * the paid license agreement will prevail.  Otherwise, the software is 
 * provided under the following open source license terms:
 * 
 * Alfresco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Alfresco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Alfresco. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.uoc.fegor.tfg.masci.mw.domain.alfresco;

import java.util.List;

/**
 * @author FeGoR [fegor@uoc.edu]
 * @version 01.00.000
 * @created 21 nov. 2019
 * @modified 21 nov. 2019 by FeGoR [fegor@uoc.edu]
 * @file People.java
 * 
 *       Clase que contiene el conjunto de items de datos de los usuarios.
 */
public class People {

	List<PeopleItems> people;
	PeoplePaging paging;

	public List<PeopleItems> getPeople() {
		return people;
	}

	public void setPeople(List<PeopleItems> people) {
		this.people = people;
	}

	public PeoplePaging getPaging() {
		return paging;
	}

	public void setPaging(PeoplePaging paging) {
		this.paging = paging;
	}
}
