/*
 *****************************************************************************
 * MASCI
 *****************************************************************************
 * Copyright (C) 2019-2020 MASCI authors
 *
 * Authors: Fernando González Ruano [fegor@uoc.edu]
 *
 * This file is part of the Alfresco software. 
 * If the software was purchased under a paid Alfresco license, the terms of 
 * the paid license agreement will prevail.  Otherwise, the software is 
 * provided under the following open source license terms:
 * 
 * Alfresco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Alfresco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Alfresco. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.uoc.fegor.tfg.masci.mw.domain.alfresco.audit;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author FeGoR [fegor@uoc.edu]
 * @version 01.00.000
 * @created 24 nov. 2019
 * @modified 24 nov. 2019 by FeGoR [fegor@uoc.edu]
 * @file Values.java
 *
 */
public class Values {
	private final static String ALF_ACCESS_TR = "/alfresco-access/transaction/";

	@JsonProperty(ALF_ACCESS_TR + "sub-actions")
	private String subActions;
	@JsonProperty(ALF_ACCESS_TR + "version")
	private String version;
	@JsonProperty(ALF_ACCESS_TR + "aspects/add")
	private String aspects;
	@JsonProperty(ALF_ACCESS_TR + "move/from/path")
	private String moveFromPath;
	@JsonProperty(ALF_ACCESS_TR + "path")
	private String path;
	@JsonProperty(ALF_ACCESS_TR + "action")
	private String action;
	@JsonProperty(ALF_ACCESS_TR + "properties/add")
	private String propertiesAdd;
	@JsonProperty(ALF_ACCESS_TR + "properties/to")
	private String propertiesTo;
	@JsonProperty(ALF_ACCESS_TR + "properties/from")
	private String propertiesFrom;
	@JsonProperty(ALF_ACCESS_TR + "type")
	private String type;
	@JsonProperty(ALF_ACCESS_TR + "user")
	private String user;

	public Values() {
	}

	public Values(String subActions, String version, String aspects, String path, String action, String propertiesAdd,
			String propertiesTo, String propertiesFrom, String type, String user) {
		super();
		this.subActions = subActions;
		this.version = version;
		this.aspects = aspects;
		this.path = path;
		this.action = action;
		this.propertiesAdd = propertiesAdd;
		this.propertiesTo = propertiesTo;
		this.propertiesFrom = propertiesFrom;
		this.type = type;
		this.user = user;
	}

	public String getSubActions() {
		return subActions;
	}

	public void setSubActions(String subActions) {
		this.subActions = subActions;
	}

	public String getAspects() {
		return aspects;
	}

	public void setAspects(String aspects) {
		this.aspects = aspects;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getPropertiesAdd() {
		return propertiesAdd;
	}

	public void setPropertiesAdd(String propertiesAdd) {
		this.propertiesAdd = propertiesAdd;
	}

	public String getMoveFromPath() {
		return moveFromPath;
	}

	public void setMoveFromPath(String moveFromPath) {
		this.moveFromPath = moveFromPath;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getPropertiesTo() {
		return propertiesTo;
	}

	public void setPropertiesTo(String propertiesTo) {
		this.propertiesTo = propertiesTo;
	}

	public String getPropertiesFrom() {
		return propertiesFrom;
	}

	public void setPropertiesFrom(String propertiesFrom) {
		this.propertiesFrom = propertiesFrom;
	}
}
