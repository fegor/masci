/*
 *****************************************************************************
 * MASCI
 *****************************************************************************
 * Copyright (C) 2019-2020 MASCI authors
 *
 * Authors: Fernando González Ruano [fegor@uoc.edu]
 *
 * This file is part of the Alfresco software. 
 * If the software was purchased under a paid Alfresco license, the terms of 
 * the paid license agreement will prevail.  Otherwise, the software is 
 * provided under the following open source license terms:
 * 
 * Alfresco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Alfresco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Alfresco. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.uoc.fegor.tfg.masci.mw.domain.neo4j;

import java.util.ArrayList;
import java.util.List;

import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author FeGoR [fegor@uoc.edu]
 * @version 01.00.000
 * @created 4 nov. 2019
 * @modified 4 nov. 2019 by FeGoR [fegor@uoc.edu]
 * @filename Container.java
 *
 */
@NodeEntity
public class Container {
	
	@Id
	@GeneratedValue
	private Long id;	
	private String uuid;
	private String name;
	private String title;
	private String path;
	
	@JsonIgnoreProperties("createdContainer")
	@Relationship(type = "CREATED", direction = Relationship.INCOMING)
	private List<CreatedContainer> createdContainer;

	@JsonIgnoreProperties("readedContainer")
	@Relationship(type = "READED", direction = Relationship.INCOMING)
	private List<ReadedContainer> readedContainer;
	
	@JsonIgnoreProperties("updatedContainer")
	@Relationship(type = "UPDATED", direction = Relationship.INCOMING)
	private List<UpdatedContainer> updatedContainer;
	
	/**
	 * 
	 */
	public Container() {
		super();
	}

	/**
	 * @param uuid
	 * @param name
	 * @param title
	 * @param path
	 */
	public Container(String uuid, String name, String title, String path) {
		super();
		this.uuid = uuid;
		this.name = name;
		this.title = title;
		this.path = path;
	}

	/**
	 * @param createdContainer
	 */
	public void addCreatedContainer(CreatedContainer createdContainer) {
		if (this.createdContainer == null) {
			this.createdContainer = new ArrayList<>();
		}

		this.createdContainer.add(createdContainer);
	}

	/**
	 * @param readedContainer
	 */
	public void addReadedContainer(ReadedContainer readedContainer) {
		if (this.readedContainer == null) {
			this.readedContainer = new ArrayList<>();
		}

		this.readedContainer.add(readedContainer);
	}
	
	/**
	 * @param updatedContainer
	 */
	public void addUpdatedContainer(UpdatedContainer updatedContainer) {
		if (this.updatedContainer == null) {
			this.updatedContainer = new ArrayList<>();
		}

		this.updatedContainer.add(updatedContainer);
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	@JsonIgnore
	public List<CreatedContainer> getCreatedContainer() {
		return createdContainer;
	}

	@JsonIgnore
	public void setCreatedContainer(List<CreatedContainer> createdContainer) {
		this.createdContainer = createdContainer;
	}

	@JsonIgnore
	public List<ReadedContainer> getReadedContainer() {
		return readedContainer;
	}

	@JsonIgnore
	public void setReadedContainer(List<ReadedContainer> readedContainer) {
		this.readedContainer = readedContainer;
	}

	@JsonIgnore
	public List<UpdatedContainer> getUpdatedContainer() {
		return updatedContainer;
	}
	
	@JsonIgnore
	public void setUpdatedContainer(List<UpdatedContainer> updatedContainer) {
		this.updatedContainer = updatedContainer;
	}
}
