/*
 *****************************************************************************
 * MASCI
 *****************************************************************************
 * Copyright (C) 2019-2020 MASCI authors
 *
 * Authors: Fernando González Ruano [fegor@uoc.edu]
 *
 * This file is part of the Alfresco software. 
 * If the software was purchased under a paid Alfresco license, the terms of 
 * the paid license agreement will prevail.  Otherwise, the software is 
 * provided under the following open source license terms:
 * 
 * Alfresco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Alfresco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Alfresco. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.uoc.fegor.tfg.masci.mw.neo4j;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Properties;

import org.apache.chemistry.opencmis.client.api.Folder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import edu.uoc.fegor.tfg.masci.mw.domain.alfresco.People;
import edu.uoc.fegor.tfg.masci.mw.domain.alfresco.audit.Access;
import edu.uoc.fegor.tfg.masci.mw.domain.neo4j.Container;
import edu.uoc.fegor.tfg.masci.mw.domain.neo4j.Content;
import edu.uoc.fegor.tfg.masci.mw.domain.neo4j.User;
import edu.uoc.fegor.tfg.masci.mw.exception.FolderNotFoundException;
import edu.uoc.fegor.tfg.masci.mw.model.AlfrescoModel;
import edu.uoc.fegor.tfg.masci.mw.services.AlfrescoCmisService;
import edu.uoc.fegor.tfg.masci.mw.services.AlfrescoPeopleService;
import edu.uoc.fegor.tfg.masci.mw.services.Neo4jService;

/**
 * @author FeGoR [fegor@uoc.edu]
 * @version 01.00.000
 * @created 24 nov. 2019
 * @modified 24 nov. 2019 by FeGoR [fegor@uoc.edu]
 * @file Neo4jManager.java
 *
 *       Gestión de las llamadas a Alfresco y Neo4j para gestionar la
 *       persistencia de nodos y relaciones leídas de la auditoría de Alfresco
 *       Community.
 */
@Service
public class Neo4jManager {
	private static final Logger LOG = LoggerFactory.getLogger(Neo4jService.class);
	private static final String USER_NOT_FOUND = "User for this content not found... creating...";
	private static final String ERROR_CREATING_USER = "Error creating user from content!";
	private static final String NOT_PROS_READED = "No properties readed!";

	@Value("${alfresco.audit.dateFormat}")
	private String dateFormat;

	@Autowired
	private Neo4jService neo4jService;

	@Autowired
	private AlfrescoCmisService alfrescoCmisService;

	@Autowired
	private AlfrescoPeopleService alfrescoPeopleService;

	/**
	 * @param access
	 * @throws ParseException
	 */
	public void createContent(Access access) throws ParseException {
		// Lectura de las propiedades recogidas en la auditoría.

		Properties properties = buildPropertiesFromValuesString(access.getValues().getPropertiesAdd());

		String username = access.getUser();
		String path = access.getValues().getPath();

		String uuid = null;
		String name = null;
		String title = null;

		if (properties != null) {
			uuid = properties.getProperty(AlfrescoModel.ALF_AUDIT_PROP_UUID);
			name = properties.getProperty(AlfrescoModel.ALF_AUDIT_PROP_NAME);
			title = properties.getProperty(AlfrescoModel.ALF_AUDIT_PROP_TITLE);
		}

		Content content = neo4jService.contentCreate(uuid, name, title, path);

		User user = neo4jService.userFindByUsername(username);

		// Si no se encuentra el usuario es porque no se ha creado en la base de datos
		// de grafos cuando se creó en Alfresco y por tanto, tiene que crearlo en este
		// momento.

		if (user == null) {
			LOG.warn(USER_NOT_FOUND);
			this.createUserFromAlfresco(username);

			// Reitera la búsqueda y si no, error y retorno.

			user = neo4jService.userFindByUsername(username);

			if (user == null) {
				LOG.error(ERROR_CREATING_USER);
				return;
			}
		}

		neo4jService.userCreatedContent(user, content, (new SimpleDateFormat(dateFormat)).parse(access.getTime()));

		if (LOG.isDebugEnabled()) {
			LOG.debug("Created content: {} from user: {}", content.getName(), user.getUsername());
		}

		// Ahora se busca el contenedor por el path y se añade a la relación. Si no se
		// encuentra, se realiza una petición vía CMIS a Alfresco para obtener los datos
		// del contenedor.

		String containerPath = path.substring(0, path.lastIndexOf('/'));
		Container container = neo4jService.containerFindByPath(containerPath);

		if (container == null) {
			container = this.findAndCreateContainer(containerPath);
		}

		// Se vuelve a comprobar, ahora no debería ser null y por tanto crear la
		// relación, si no, se avisa que ha sido imposible realizarla.

		if (container != null) {
			neo4jService.contentContainedInContainer(container, content);

			if (LOG.isDebugEnabled()) {
				LOG.debug("Create relationship with '{}' between '{}'", container.getPath(), content.getPath());
			}

		} else {
			LOG.error("Relationship not creted!");
		}
	}

	/**
	 * @param access
	 */
	public void deleteContent(Access access) {
		// Se busca el contenido a borrar

		Content content = neo4jService.contentFindByPath(access.getValues().getPath());

		// Si se ha encontrado, se borra

		if (content != null) {
			neo4jService.contentDelete(content);

			if (LOG.isDebugEnabled()) {
				LOG.debug("Delete content with path: '{}'", access.getValues().getPath());
			}
		}
	}

	/**
	 * @param access
	 * @throws ParseException
	 */
	public void updateContent(Access access) throws ParseException {
		String username = access.getUser();

		String path = null;

		if (access.getValues().getAction().equals(AlfrescoModel.ALF_AUDIT_ACTION_MOVE)) {
			path = access.getValues().getMoveFromPath();

		} else {
			path = access.getValues().getPath();
		}

		// Se busca el contenido y si no existe es que no se ha encontrado cuando se
		// creó

		// TODO Aquí, depende de si la actualización es del contenido o de alguna
		// propiedad pero no se va a modificar, simplemente se crea la relación de que
		// ha sido actualizado. Más adelante, habría que modficar las propiedades que se
		// hayan modificado y que hayan sido guardadas en la base de datos de gráfos.

		Content content = neo4jService.contentFindByPath(path);

		if (content == null) {
			LOG.warn("Content updated not found!");
			return;

		} else if (access.getValues().getAction().equals(AlfrescoModel.ALF_AUDIT_ACTION_UPDATE_NODE_PROPERTIES)
				|| access.getValues().getAction().equals(AlfrescoModel.ALF_AUDIT_ACTION_MOVE)) {
			Properties props = buildPropertiesFromValuesString(access.getValues().getPropertiesTo());

			if (props != null) {
				if (props.getProperty(AlfrescoModel.ALF_AUDIT_PROP_NAME) != null) {
					content.setName(props.getProperty(AlfrescoModel.ALF_AUDIT_PROP_NAME));
				}

				if (props.getProperty(AlfrescoModel.ALF_AUDIT_PROP_TITLE) != null) {
					content.setTitle(props.getProperty(AlfrescoModel.ALF_AUDIT_PROP_TITLE));
				}
			}

			if (access.getValues().getAction().equals(AlfrescoModel.ALF_AUDIT_ACTION_MOVE)) {
				content.setPath(path);
			}
		}

		User user = neo4jService.userFindByUsername(username);

		// Si no se encuentra el usuario es porque no se ha creado en la base de datos
		// de grafos cuando se creó en Alfresco y por tanto, tiene que crearlo en este
		// momento.

		if (user == null) {
			LOG.warn(USER_NOT_FOUND);
			this.createUserFromAlfresco(username);

			// Reitera la búsqueda y si no, error y retorno.

			user = neo4jService.userFindByUsername(username);

			if (user == null) {
				LOG.error(ERROR_CREATING_USER);
				return;
			}
		}

		neo4jService.userUpdatedContent(user, content, (new SimpleDateFormat(dateFormat)).parse(access.getTime()));

		if (LOG.isDebugEnabled()) {
			LOG.debug("Updated content: {} from user: {}", content.getName(), user.getUsername());
		}
	}

	/**
	 * @param access
	 * @throws ParseException
	 */
	public void readContent(Access access) throws ParseException {
		String username = access.getUser();
		String path = access.getValues().getPath();

		// Se localiza el contenido (que debería de estar creado ya que es una lectura)
		// a través del path ya que esta entrada en la auditoría no contiene el UUID del
		// nodo.

		Content content = neo4jService.contentFindByPath(path);

		// TODO Si el contenido no se encuentra puede ser por que no se ha leído toda la
		// auditoría, hay que crearlo leyendo directamente la información vía CMIS.

		if (content == null) {
			LOG.error("Content not found!: path = {}", path);
			return;
		}

		// Ahora se localiza el usuario, en este caso podría no estar creado en la base
		// de datos ya que puede haber leído la información de la auditoría después de
		// haberse creado el usuario. Si no se localiza, se crea realizando una consulta
		// a Alfresco.

		User user = neo4jService.userFindByUsername(username);

		if (user == null) {
			LOG.warn(USER_NOT_FOUND);
			this.createUserFromAlfresco(username);

			// Reitera la búsqueda y si no, error y retorno.

			user = neo4jService.userFindByUsername(username);

			if (user == null) {
				LOG.error(ERROR_CREATING_USER);
				return;
			}
		}

		// Ahora se realiza la relación de lectura del usuario al contenido.

		neo4jService.userReadedContent(user, content, (new SimpleDateFormat(dateFormat)).parse(access.getTime()));
	}

	/**
	 * @param path
	 * @return
	 */
	private Container findAndCreateContainer(String path) {
		Container container = null;
		Folder folder = null;

		String xPath = path.substring(0, path.lastIndexOf("cm:") - 1);

		try {
			folder = alfrescoCmisService.getFolderByXPath(xPath.replace(" ", "_x0020_"));
			container = new Container();

			if (folder != null) {
				container.setUuid(folder.getId().substring(0, 36));
				container.setName(folder.getName());
				container.setTitle(folder.getProperty("cm:title").getValueAsString());
				container.setPath(path);

				if (LOG.isDebugEnabled()) {
					LOG.debug("Container created with CMIS [UUID: {}, name: {}, title: {}, path {}]",
							container.getUuid(), container.getName(), container.getTitle(), container.getPath());
				}
			}

		} catch (FolderNotFoundException e) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Alfresco folder not found!");
			}
		}

		return container;
	}

	/**
	 * @param access
	 * @throws ParseException
	 */
	public void createContainer(Access access) throws ParseException {
		// Lectura de las propiedades recogidas en la auditoría.

		Properties properties = buildPropertiesFromValuesString(access.getValues().getPropertiesAdd());

		String username = access.getUser();
		String path = access.getValues().getPath();

		String uuid = null;
		String name = null;
		String title = null;

		if (properties != null) {
			uuid = properties.getProperty(AlfrescoModel.ALF_AUDIT_PROP_UUID);
			name = properties.getProperty(AlfrescoModel.ALF_AUDIT_PROP_NAME);
			title = properties.getProperty(AlfrescoModel.ALF_AUDIT_PROP_TITLE);
		}

		if (LOG.isDebugEnabled()) {
			LOG.debug("Content created: uuid = {}, name = {}, title = {}, path = {}, date = {}", uuid, name, title,
					path, access.getTime());
		}

		Container container = neo4jService.containerCreate(uuid, name, title, path);

		User user = neo4jService.userFindByUsername(username);

		// Si no se encuentra el usuario es porque no se ha creado en la base de datos
		// de grafos cuando se creó en Alfresco y por tanto, tiene que crearlo en este
		// momento.

		if (user == null) {
			LOG.warn(USER_NOT_FOUND);
			this.createUserFromAlfresco(username);

			// Reitera la búsqueda y si no, error y retorno.

			user = neo4jService.userFindByUsername(username);

			if (user == null) {
				LOG.error(ERROR_CREATING_USER);
				return;
			}
		}

		neo4jService.userCreatedContainer(user, container, (new SimpleDateFormat(dateFormat)).parse(access.getTime()));

		if (LOG.isDebugEnabled()) {
			LOG.debug("Created container: {} from user: {}", container.getName(), user.getUsername());
		}
	}

	/**
	 * @param access
	 */
	public void deleteContainer(Access access) {
		// Se busca el contenedor a borrar

		Container container = neo4jService.containerFindByPath(access.getValues().getPath());

		// Si se ha encontrado, se borra

		if (container != null) {
			neo4jService.containerDelete(container);

			if (LOG.isDebugEnabled()) {
				LOG.debug("Delete container with path: '{}'", access.getValues().getPath());
			}
		}
	}

	/**
	 * @param access
	 * @throws ParseException
	 */
	public void updateContainer(Access access) throws ParseException {
		String username = access.getUser();

		String path = null;

		if (access.getValues().getAction().equals(AlfrescoModel.ALF_AUDIT_ACTION_MOVE)) {
			path = access.getValues().getMoveFromPath();

		} else {
			path = access.getValues().getPath();
		}

		// Se busca el contenedor y si no existe es que no se ha encontrado cuando se
		// creó

		Container container = neo4jService.containerFindByPath(path);

		if (container == null) {
			LOG.warn("Container updated not found!");
			return;

			// Si se han actualizado las propiedades o se ha movido la carpeta se actualizan
			// las propiedades y si, además, se ha movido, se asigna el nuevo path.

		} else if (access.getValues().getAction().equals(AlfrescoModel.ALF_AUDIT_ACTION_UPDATE_NODE_PROPERTIES)
				|| access.getValues().getAction().equals(AlfrescoModel.ALF_AUDIT_ACTION_MOVE)) {
			Properties props = buildPropertiesFromValuesString(access.getValues().getPropertiesTo());

			if (props != null) {
				if (props.getProperty(AlfrescoModel.ALF_AUDIT_PROP_NAME) != null) {
					container.setName(props.getProperty(AlfrescoModel.ALF_AUDIT_PROP_NAME));
				}

				if (props.getProperty(AlfrescoModel.ALF_AUDIT_PROP_TITLE) != null) {
					container.setTitle(props.getProperty(AlfrescoModel.ALF_AUDIT_PROP_TITLE));
				}
			}

			if (access.getValues().getAction().equals(AlfrescoModel.ALF_AUDIT_ACTION_MOVE)) {
				container.setPath(path);
			}
		}

		User user = neo4jService.userFindByUsername(username);

		// Si no se encuentra el usuario es porque no se ha creado en la base de datos
		// de grafos cuando se creó en Alfresco y por tanto, tiene que crearlo en este
		// momento.

		// TODO refactorizar, ya que este código ser repite. Usar una función privada.

		if (user == null) {
			LOG.warn(USER_NOT_FOUND);
			this.createUserFromAlfresco(username);

			// Reitera la búsqueda y si no, error y retorno.

			user = neo4jService.userFindByUsername(username);

			if (user == null) {
				LOG.error(ERROR_CREATING_USER);
				return;
			}
		}

		neo4jService.userUpdatedContainer(user, container, (new SimpleDateFormat(dateFormat)).parse(access.getTime()));

		if (LOG.isDebugEnabled()) {
			LOG.debug("Updated container: {} from user: {}", container.getName(), user.getUsername());
		}
	}

	/**
	 * @param access
	 * @throws ParseException
	 */
	public void readContainer(Access access) throws ParseException {
		String username = access.getUser();
		String path = access.getValues().getPath();

		// Se localiza el folder/container (que debería de estar creado ya que es una
		// lectura) a través del path ya que esta entrada en la auditoría no contiene el
		// UUID del nodo.

		Container container = neo4jService.containerFindByPath(path);

		if (container == null) {
			LOG.error("Container not found!: path = {}", path);
			return;
		}

		// Ahora se localiza el usuario, en este caso podría no estar creado en la base
		// de datos ya que puede haber leído la información de la auditoría después de
		// haberse creado el usuario. Si no se localiza, se crea realizando una consulta
		// a Alfresco.

		User user = neo4jService.userFindByUsername(username);

		if (user == null) {
			LOG.warn(USER_NOT_FOUND);
			this.createUserFromAlfresco(username);

			// Reitera la búsqueda y si no, error y retorno.

			user = neo4jService.userFindByUsername(username);

			if (user == null) {
				LOG.error(ERROR_CREATING_USER);
				return;
			}
		}

		// Ahora se realiza la relación de lectura del usuario al contenido.

		neo4jService.userReadedContainer(user, container, (new SimpleDateFormat(dateFormat)).parse(access.getTime()));
	}

	/**
	 * @param userName
	 */
	private void createUserFromAlfresco(String user) {
		People people = this.alfrescoPeopleService.getPeople(user);

		if (people == null) {
			return;
		}

		String uuid = null;
		String userName = people.getPeople().get(0).getUserName();
		String firstName = people.getPeople().get(0).getFirstName();
		String lastName = people.getPeople().get(0).getLastName();
		String email = people.getPeople().get(0).getEmail();

		neo4jService.userCreate(uuid, userName, firstName, lastName, email);
	}

	/**
	 * @param access
	 */
	public void createUser(Access access) {
		Properties properties = buildPropertiesFromValuesString(access.getValues().getPropertiesAdd());

		if (properties == null) {
			LOG.error(NOT_PROS_READED);
			return;
		}

		String uuid = properties.getProperty(AlfrescoModel.ALF_AUDIT_PROP_UUID);
		String userName = properties.getProperty(AlfrescoModel.ALF_AUDIT_PROP_PERSON_USERNAME);
		String firstName = properties.getProperty(AlfrescoModel.ALF_AUDIT_PROP_FIRSTNAME);
		String lastName = properties.getProperty(AlfrescoModel.ALF_AUDIT_PROP_LASTNAME);
		String email = properties.getProperty(AlfrescoModel.ALF_AUDIT_PROP_EMAIL);

		neo4jService.userCreate(uuid, userName, firstName, lastName, email);
	}

	/**
	 * @param s
	 * @return
	 */
	private Properties buildPropertiesFromValuesString(String s) {
		if (s == null || s.length() == 0) {
			return null;
		}

		Properties props = new Properties();
		String[] pairs = (s.substring(1, s.length() - 1)).split(",");

		for (String pair : pairs) {
			String[] nameAndValue = pair.split("=", 2);
			props.setProperty(nameAndValue[0].trim(), nameAndValue.length > 1 ? nameAndValue[1].trim() : "");
		}

		return props;
	}

	public Neo4jService getNeo4jService() {
		return neo4jService;
	}

	public void setNeo4jService(Neo4jService neo4jService) {
		this.neo4jService = neo4jService;
	}
}
