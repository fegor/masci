/*
 *****************************************************************************
 * MASCI
 *****************************************************************************
 * Copyright (C) 2019-2020 MASCI authors
 *
 * Authors: Fernando González Ruano [fegor@uoc.edu]
 *
 * This file is part of the Alfresco software. 
 * If the software was purchased under a paid Alfresco license, the terms of 
 * the paid license agreement will prevail.  Otherwise, the software is 
 * provided under the following open source license terms:
 * 
 * Alfresco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Alfresco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Alfresco. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.uoc.fegor.tfg.masci.mw;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import edu.uoc.fegor.tfg.masci.mw.domain.neo4j.LastAudit;
import edu.uoc.fegor.tfg.masci.mw.services.LastAuditService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional
public class LastAuditInDBTest {
	private static final Logger LOG = LoggerFactory.getLogger(LastAuditInDBTest.class);

	@Autowired
	private LastAuditService lastAuditService;

	@Before
	public void setUp() {
		
	}

	/**
	 * 
	 */
	@Test
	public void testReadAndSaveObject() {
		LastAudit lastAudit = lastAuditService.getLastAudit();
		
		if (lastAudit == null) {
			lastAudit = new LastAudit("0", "1/1/1970");
			lastAuditService.saveLastAudit(lastAudit);
			
		} else {
			String id = lastAudit.getAccessId();
			long lid = Long.parseLong(id);
			lid++;
			id = String.valueOf(lid);
			
			lastAudit.setAccessId(id);
			
			
		    Date date = Calendar.getInstance().getTime();  
		    DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");  
		    String strDate = dateFormat.format(date); 
		    
		    lastAudit.setFromDate(strDate);

		    lastAuditService.saveLastAudit(lastAudit);
		}
		
		LOG.info(lastAudit.toString());

	}

}
