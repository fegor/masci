/*
 *****************************************************************************
 * MASCI
 *****************************************************************************
 * Copyright (C) 2019-2020 MASCI authors
 *
 * Authors: Fernando González Ruano [fegor@uoc.edu]
 *
 * This file is part of the Alfresco software. 
 * If the software was purchased under a paid Alfresco license, the terms of 
 * the paid license agreement will prevail.  Otherwise, the software is 
 * provided under the following open source license terms:
 * 
 * Alfresco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Alfresco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Alfresco. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.uoc.fegor.tfg.masci.mw;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Collection;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import edu.uoc.fegor.tfg.masci.mw.domain.neo4j.Content;
import edu.uoc.fegor.tfg.masci.mw.domain.neo4j.CreatedContent;
import edu.uoc.fegor.tfg.masci.mw.domain.neo4j.User;
import edu.uoc.fegor.tfg.masci.mw.repository.ContentRepository;
import edu.uoc.fegor.tfg.masci.mw.repository.UserRepository;

/**
 * @author FeGoR [fegor@uoc.edu]
 * @version 01.00.000
 * @created 4 nov. 2019
 * @modified 4 nov. 2019 by FeGoR [fegor@uoc.edu]
 * @filename ContentRepositoryTest.java
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional
public class ContentRepositoryTest {

	@Autowired
	private ContentRepository contentRepository;

	@Autowired
	private UserRepository userRepository;

	/**
	 * 
	 */
	@Before
	public void setUp() {
		Content doc1 = new Content("C1", "doc1.docx", "Document 1", "/path");

		contentRepository.save(doc1);

		User pgrillo = new User("U1", "pgrillo", "Pepito", "Grillo", "pgrillo@nuncajamas.org");

		userRepository.save(pgrillo);

		CreatedContent createDoc = new CreatedContent(pgrillo, doc1);
//		createDoc.addCreatedContent("create");
		createDoc.addCreatedContent(new Date());

		doc1.addCreatedContent(createDoc);

		contentRepository.save(doc1);
	}

	/**
	 * 
	 */
	@Test
	public void testFindByTitleContaining() {
		String title = "*Document*";
		Collection<Content> result = contentRepository.findByTitleLike(title);
		assertNotNull(result);
		assertEquals(1, result.size());
	}

	/**
	 * 
	 */
	@Test
	public void testGraph() {
		Collection<Content> graph = contentRepository.graph(5);

		assertEquals(1, graph.size());

		Content content = graph.iterator().next();

		assertEquals(1, content.getCreatedContent().size());

		assertEquals("Document 1", content.getTitle());
		assertEquals("pgrillo", content.getCreatedContent().iterator().next().getUser().getUsername());
	}

	@Test
	public void testDelete() {

	}
}
