/*
 *****************************************************************************
 * MASCI
 *****************************************************************************
 * Copyright (C) 2019-2020 MASCI authors
 *
 * Authors: Fernando González Ruano [fegor@uoc.edu]
 *
 * This file is part of the Alfresco software. 
 * If the software was purchased under a paid Alfresco license, the terms of 
 * the paid license agreement will prevail.  Otherwise, the software is 
 * provided under the following open source license terms:
 * 
 * Alfresco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Alfresco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Alfresco. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.uoc.fegor.tfg.masci.mw;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;

import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional
public class UtilTest {
	private static final Logger LOG = LoggerFactory.getLogger(UtilTest.class);

	/**
	 * @throws IOException
	 * @throws JSONException
	 */
	@Test
	public void testUtil() throws IOException, JSONException {
		String prueba = "{{http://www.alfresco.org/model/content/1.0}created=Thu Nov 14 11:34:05 CET 2019, {http://www.alfresco.org/model/content/1.0}description={es=Document Library}, {http://www.alfresco.org/model/content/1.0}creator=admin, {http://www.alfresco.org/model/system/1.0}node-uuid=1e2e1d2a-0cc9-49d4-b88e-5241c3eab07a, {http://www.alfresco.org/model/content/1.0}name=documentLibrary, {http://www.alfresco.org/model/system/1.0}store-protocol=workspace, {http://www.alfresco.org/model/system/1.0}store-identifier=SpacesStore, {http://www.alfresco.org/model/system/1.0}node-dbid=914, {http://www.alfresco.org/model/system/1.0}locale=es, {http://www.alfresco.org/model/content/1.0}modifier=admin, {http://www.alfresco.org/model/site/1.0}componentId=documentLibrary, {http://www.alfresco.org/model/content/1.0}owner=admin, {http://www.alfresco.org/model/content/1.0}modified=Thu Nov 14 11:34:05 CET 2019}";

		Properties properties = this.buildPropertiesFromValuesString(prueba);
		Enumeration<Object> em = properties.keys();

		if (LOG.isDebugEnabled()) {
			LOG.debug("Properties:");
		}

		while (em.hasMoreElements()) {
			String str = (String) em.nextElement();

			if (LOG.isDebugEnabled()) {
				LOG.debug("    {} : {}", str, properties.get(str));
			}
		}

		assertEquals(properties.get("{http://www.alfresco.org/model/system/1.0}node-uuid"),
				"1e2e1d2a-0cc9-49d4-b88e-5241c3eab07a");
	}

	/**
	 * @param s
	 * @return
	 * @throws IOException
	 * @throws JSONException
	 * @throws ClassNotFoundException
	 */
	private Properties buildPropertiesFromValuesString(String s) {
		Properties props = new Properties();
		String[] pairs = (s.substring(1, s.length() - 1)).split(",");

		for (String pair : pairs) {
			String[] nameAndValue = pair.split("=", 2);
			props.setProperty(nameAndValue[0].trim(), nameAndValue.length > 1 ? nameAndValue[1].trim() : "");
		}

		return props;
	}
}
