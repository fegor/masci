/*
 *****************************************************************************
 * MASCI
 *****************************************************************************
 * Copyright (C) 2019-2020 MASCI authors
 *
 * Authors: Fernando González Ruano [fegor@uoc.edu]
 *
 * This file is part of the Alfresco software. 
 * If the software was purchased under a paid Alfresco license, the terms of 
 * the paid license agreement will prevail.  Otherwise, the software is 
 * provided under the following open source license terms:
 * 
 * Alfresco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Alfresco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Alfresco. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.uoc.fegor.tfg.masci.mw;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import edu.uoc.fegor.tfg.masci.mw.exception.DocumentNotFoundException;
import edu.uoc.fegor.tfg.masci.mw.exception.FolderNotFoundException;
import edu.uoc.fegor.tfg.masci.mw.model.AlfrescoModel;
import edu.uoc.fegor.tfg.masci.mw.services.AlfrescoCmisService;
import junit.framework.TestCase;

/**
 * @author FeGoR <fegor@ms2sgroup.com>
 * @version 01.00.000
 * @created 2019-03-22 13:55
 * @modified 2019-03-22 13:55 by FeGoR <fegor@ms2sgroup.com>
 * @filename AlfrescoTest.java
 *
 */
@RunWith(SpringRunner.class)
@EnableAutoConfiguration
@SpringBootTest
public class AlfrescoCmisTest extends TestCase {
	private static final Logger LOG = LoggerFactory.getLogger(AlfrescoCmisTest.class);

	private final static String FILE_NAME = "test.txt";
	private final static String FOLDER_PATH = "/";
	private final static String MESSAGE_CONTENT = "Hello, World!";

	@Autowired
	AlfrescoCmisService alfrescoCmisService;

	Session session;

	@Before
	public void setUp() {
		session = alfrescoCmisService.createSession();
		assertNotNull(session);
	}

	@Test
	public void testListDocuments() {
		assertTrue(alfrescoCmisService.contentItems().size() > 0);
	}

	@Test
	public void testCMISDocument() throws FolderNotFoundException, IOException, DocumentNotFoundException {

		// Creación del documento en la carpeta principal

		Folder parent = alfrescoCmisService.getFolderByPath(FOLDER_PATH);
		String uuidFolder = parent.getId();
		InputStream content = new ByteArrayInputStream(MESSAGE_CONTENT.getBytes(StandardCharsets.UTF_8));

		Map<String, Object> properties = new HashMap<String, Object>();
		properties.put(PropertyIds.NAME, FILE_NAME);
		properties.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document");

		String uuidDocument = alfrescoCmisService.createDocument(FILE_NAME, parent, content, content.available(),
				"plain/text", properties);
		assertNotNull(uuidDocument);

		// Consulta vía Query CMIS

		final String query = "SELECT * FROM cmis:document WHERE cmis:name = '" + FILE_NAME + "'";
		List<CmisObject> results = alfrescoCmisService.findByCMISQuery(query);
		assertTrue(results.size() > 0);

		// Localización de documento

		LOG.info("Get document by ID: {}", AlfrescoModel.ALF_WS_PROTOCOL + uuidDocument);
		Document d = alfrescoCmisService.getDocumentByID(AlfrescoModel.ALF_WS_PROTOCOL + uuidDocument);
		assertNotNull(d);

		LOG.info("Get Folder by ID: {}", AlfrescoModel.ALF_WS_PROTOCOL + uuidFolder);
		Folder f = alfrescoCmisService.getFolderByID(AlfrescoModel.ALF_WS_PROTOCOL + uuidFolder);
		assertNotNull(f);

		// Borrado del documento

		d.delete();
	}
}
