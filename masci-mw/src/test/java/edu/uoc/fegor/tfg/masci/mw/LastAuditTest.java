/*
 *****************************************************************************
 * MASCI
 *****************************************************************************
 * Copyright (C) 2019-2020 MASCI authors
 *
 * Authors: Fernando González Ruano [fegor@uoc.edu]
 *
 * This file is part of the Alfresco software. 
 * If the software was purchased under a paid Alfresco license, the terms of 
 * the paid license agreement will prevail.  Otherwise, the software is 
 * provided under the following open source license terms:
 * 
 * Alfresco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Alfresco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Alfresco. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.uoc.fegor.tfg.masci.mw;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import edu.uoc.fegor.tfg.masci.mw.domain.neo4j.LastAudit;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional
public class LastAuditTest {
	private static final Logger LOG = LoggerFactory.getLogger(LastAuditTest.class);
	private static final String FILE_NAME = "lastauditTest.obj";

	File file;

	@Before
	public void setUp() {
		this.file = new File(FILE_NAME);
	}

	/**
	 * 
	 */
	@Test
	public void testReadAndSaveObject() {
		LastAudit lastAudit = readLastAudit();
		
		if (lastAudit.getAccessId().equals("-1")) {
			lastAudit.setAccessId("1");
			lastAudit.setFromDate("1/1/1970");
			
		} else {
			String id = lastAudit.getAccessId();
			long lid = Long.parseLong(id);
			lid++;
			id = String.valueOf(lid);
			
			lastAudit.setAccessId(id);
			
			
		    Date date = Calendar.getInstance().getTime();  
		    DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");  
		    String strDate = dateFormat.format(date);  
		    
		    lastAudit.setFromDate(strDate);
		}
		
		this.saveLastAudit(lastAudit);
		LOG.info(lastAudit.toString());

	}
	
	/**
	 * @return
	 */
	private LastAudit readLastAudit() {
		LastAudit lastAudit = null;
		FileInputStream fileInput;
		ObjectInputStream objectInput;
		
		try {
			fileInput = new FileInputStream(this.file);
			
			objectInput = new ObjectInputStream(fileInput);
			lastAudit = (LastAudit) objectInput.readObject();
			
			objectInput.close();
			fileInput.close();
			
		} catch (FileNotFoundException e) {
			LOG.info("File not found. Create in next pass.");
			lastAudit = new LastAudit("-1", "");
			
		} catch (IOException e) {
			LOG.error("File error I/O: {0}", e.getMessage());
			
		} catch (ClassNotFoundException e) {
			LOG.error("Error read class object: {0}", e.getMessage());
		}
		
		return lastAudit;
	}
	
	/**
	 * @param lastAudit
	 */
	private void saveLastAudit(LastAudit lastAudit) {
		FileOutputStream fileOutput;
		ObjectOutputStream objectOutput;
		
		try {
			fileOutput = new FileOutputStream(this.file);
			
			objectOutput = new ObjectOutputStream(fileOutput);
			objectOutput.writeObject(lastAudit);
			
			objectOutput.close();
			fileOutput.close();
			
		} catch (FileNotFoundException e) {
			LOG.error("File not found. Create in next pass.");
			
		} catch (IOException e) {
			LOG.error("File error I/O: {0}", e.getMessage());
		}
	}

}
