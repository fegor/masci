<@markup id="css" >
   <#-- CSS Dependencies -->
</@>

<@markup id="js">
   <#-- JavaScript Dependencies -->
</@>

<@markup id="widgets">
   <@createWidgets group="dashlets"/>
</@>

<@markup id="post">
</@>

<@markup id="html">
   <@uniqueIdDiv>
      <div class="dashlet">
         <div class="title">${msg("header.label")}</div>
         <div class="body dashlet-padding">
         
         <#-- Si el resultado no es null y tiene contenido -->
         
            <#if result.documents?? && result.documents?has_content>
            
            <#-- Se realiza un bucle para todos los contenidos recogidos -->
            
                <#list result.documents as content>
                    <ol>
                        <#if content.name?has_content>
                        
                        <#-- Solamente se visualizan los que no sean páginas ni el dashboard -->
                        
                            <#if content.name?substring(0, 5) != "page." && content.name != "dashboard.xml">
                                <li>
                                
                                <#-- Se construye el enlace a la previsualización del documento -->
                                
                                <a href="${url.context}/page/site/prueba/document-details?nodeRef=workspace://SpacesStore/${content.uuid}">
                                ${content.name?if_exists}
                                </a>
                                </li>
                            </#if>
                        </#if>
                    </ol>
                </#list>
            <#else>
                <p>No data available</p>
            </#if>         
         </div>
      </div>
   </@>
</@>
