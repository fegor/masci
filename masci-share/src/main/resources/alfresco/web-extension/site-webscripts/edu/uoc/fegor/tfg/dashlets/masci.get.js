// -----------------------------------------------
// @Author	Fernando González Ruano <fegor@uoc.es>
// @Version 1.0.0
// @Date	02/12/2019
// -----------------------------------------------
function widgets() {

	// Lee la configuración y construye la cadena

	var masciConfig = new XML(config.script);
	var url = masciConfig.url + user.id;

	// Realiza la conexión y recoge el resultado

	var connector = remote.connect("http");
	var resultCall = connector.call(url);
	var result = jsonUtils.toObject(resultCall);

	// Configuración del dashlet

	var dashletTitleBarActions = {
		id : "DashletTitleBarActions",
		name : "Alfresco.widget.DashletTitleBarActions",
		useMessages : false,
		options : {
			actions : [ {
				cssClass : "help",
				bubbleOnClick : {
					message : msg.get("dashlet.help")
				},
				tooltip : msg.get("dashlet.help.tooltip")
			} ]
		}
	};

	// Construcción del modelo de resultados para enviarlo a la plantilla

	model.result = result;
	model.widgets = [ dashletTitleBarActions ];
}

widgets();
